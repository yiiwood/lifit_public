import LF
import os
import numpy as np
import scipy.ndimage.interpolation as ipol

path = os.path.abspath("./")
sep = os.path.sep

def createTestLF(shape=(5,5,512,512)):
  
  size = (shape[2],shape[3])

  test_lf = np.zeros((shape[0],shape[1],shape[2],shape[3],3),dtype=np.uint8)
  
  test_noise_1 = np.load(path+sep+"tests"+sep+"data"+sep+"test_noise_1.npy")
  test_noise_2 = np.load(path+sep+"tests"+sep+"data"+sep+"test_noise_2.npy")
  
  test_noise_1 = test_noise_1[0:size[0],0:size[1],:]
  test_noise_2 = test_noise_2[0:size[0],0:size[1]]
  
  mask = np.zeros((size[0],size[1]),dtype=np.float32)
  inverse_mask = np.ones((size[0],size[1]),dtype=np.float32)
  mask[size[0]/2-size[0]/4:size[0]/2+size[0]/4:,size[1]/2-size[1]/4:size[1]/2+size[1]/4] = 1    
          
  shifts=np.arange(-2,3,1).astype(np.float32)
  for v in range(5):
    for h in range(5):
      tmp_mask = ipol.shift(mask,shift=(shifts[v]*1.0,shifts[h]*1.0))
      inverse_mask = np.ones((size[0],size[1]),dtype=np.float32)
      inverse_mask -= tmp_mask[:]
      tmp_test_noise_2 = ipol.shift(test_noise_2,shift=(shifts[v]*1.0,shifts[h]*1.0))*tmp_mask[:]
      
      test_lf[v,h,:,:,0] = test_noise_1[:,:,0]*inverse_mask[:]+tmp_test_noise_2[:]
      test_lf[v,h,:,:,1] =test_noise_1[:,:,1]*inverse_mask[:]
      test_lf[v,h,:,:,2] =test_noise_1[:,:,2]*inverse_mask[:]
      
  lf = LF.LightField()
  lf.lf = test_lf[:]
  lf.vRes = shape[0]
  lf.hRes = shape[1]
  lf.yRes = shape[2]
  lf.xRes = shape[3]
  return lf


def compareLFs(test_lf,ref_lf):
  if test_lf.shape != ref_lf.shape:
    return False
  norm = 1
  for i in range(len(test_lf.shape)):
    norm*=test_lf.shape[i]
  print "norm2",norm
  diff = np.abs(test_lf[:]-ref_lf[:])/()
  checksum = np.sum(diff)
  print "checksum",checksum
  checksum/=float(norm)
  print "checksum",checksum