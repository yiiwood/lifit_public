import LF
from utils import *

import numpy as np
import random
import unittest
import scipy.ndimage.interpolation as ipol

from LFLib.ImageProcessing.ui import *
from scipy.misc import imsave

LF.Depth.LocalDepth.DEBUG=0
LF.Depth.LocalDepthHQ.DEBUG=0

path = os.path.abspath("./")
sep = os.path.sep


class TestLocalDepthFunctions(unittest.TestCase):

  def setUp(self):
    self.test_lf = createTestLF()
  
  def test_getSubspace(self):
    sp_v = LF.Depth.utils.getSubspace(self.test_lf.lf,v=2)
    sp_h = LF.Depth.utils.getSubspace(self.test_lf.lf,h=2)
    
    self.assertEqual(np.sum(sp_v[:]-self.test_lf.lf[:,2,:,:,:]), 0)
    self.assertEqual(np.sum(sp_h[:]-self.test_lf.lf[2,:,:,:,:]), 0)
    
    self.assertRaises(TypeError, sp_v, self.test_lf.lf)
    self.assertRaises(TypeError, sp_h, self.test_lf.lf)
  
  def test_splitAndMerge(self):
    threads = [1,2,4,8]

    LF.Depth.LocalDepth.numOfThreads = threads[0]
    pieces = LF.Depth.utils.splitLightField(self.test_lf.lf[:])
    res = LF.Depth.utils.mergeResultPieces(pieces)
    self.assertEqual(np.sum(res[:]-self.test_lf.lf[:]), 0)
    
    LF.Depth.LocalDepth.numOfThreads = threads[1]
    pieces = LF.Depth.utils.splitLightField(self.test_lf.lf[:])
    res = LF.Depth.utils.mergeResultPieces(pieces)
    self.assertEqual(np.sum(res[:]-self.test_lf.lf[:]), 0)
    
    LF.Depth.LocalDepth.numOfThreads = threads[2]
    pieces = LF.Depth.utils.splitLightField(self.test_lf.lf[:])
    res = LF.Depth.utils.mergeResultPieces(pieces)
    self.assertEqual(np.sum(res[:]-self.test_lf.lf[:]), 0)
    
    LF.Depth.LocalDepth.numOfThreads = threads[3]
    pieces = LF.Depth.utils.splitLightField(self.test_lf.lf[:])
    res = LF.Depth.utils.mergeResultPieces(pieces)
    self.assertEqual(np.sum(res[:]-self.test_lf.lf[:]), 0)
    
  def test_calcDepth_threads(self):
    threads = [1,2,4,8]
    ref_disparity = np.load(path+sep+"tests"+sep+"data"+sep+"test_LocalDepth_disparity.npy")
    
    LF.Depth.LocalDepth.numOfThreads = threads[0]
    LF.Depth.LocalDepth.calcDepth(self.test_lf,inner=0.7,outer=0.9)
    diff = np.sum(np.abs(self.test_lf.depth[2,2,:,:]-ref_disparity[:]))
    self.assertEqual(np.round(diff)<10000,True)
    
    LF.Depth.LocalDepth.numOfThreads = threads[1]
    LF.Depth.LocalDepth.calcDepth(self.test_lf,inner=0.7,outer=0.9)
    diff = np.sum(np.abs(self.test_lf.depth[2,2,:,:]-ref_disparity[:]))
    self.assertEqual(np.round(diff)<10000,True)
    
    LF.Depth.LocalDepth.numOfThreads = threads[2]
    LF.Depth.LocalDepth.calcDepth(self.test_lf,inner=0.7,outer=0.9)
    diff = np.sum(np.abs(self.test_lf.depth[2,2,:,:]-ref_disparity[:]))
    self.assertEqual(np.round(diff)<10000,True)
    
    LF.Depth.LocalDepth.numOfThreads = threads[3]
    LF.Depth.LocalDepth.calcDepth(self.test_lf,inner=0.7,outer=0.9)
    diff = np.sum(np.abs(self.test_lf.depth[2,2,:,:]-ref_disparity[:]))
    self.assertEqual(np.round(diff)<10000,True)
    
  def test_calcDepthHQ_threads(self):
    threads = [1,2,4,8]
    ref_disparity = np.load(path+sep+"tests"+sep+"data"+sep+"test_LocalDepthHQ_disparity.npy")
    
    LF.Depth.LocalDepthHQ.numOfThreads = threads[0]
    LF.Depth.LocalDepthHQ.calcDepthHQ(self.test_lf,inner=0.7,outer=0.9)
    diff = np.sum(np.abs(self.test_lf.depth[2,2,:,:]-ref_disparity[:]))
    self.assertEqual(np.round(diff)<10000,True)
    
    LF.Depth.LocalDepthHQ.numOfThreads = threads[1]
    LF.Depth.LocalDepthHQ.calcDepthHQ(self.test_lf,inner=0.7,outer=0.9)
    diff = np.sum(np.abs(self.test_lf.depth[2,2,:,:]-ref_disparity[:]))
    self.assertEqual(np.round(diff)<10000,True)
    
    LF.Depth.LocalDepthHQ.numOfThreads = threads[2]
    LF.Depth.LocalDepthHQ.calcDepthHQ(self.test_lf,inner=0.7,outer=0.9)
    diff = np.sum(np.abs(self.test_lf.depth[2,2,:,:]-ref_disparity[:]))
    self.assertEqual(np.round(diff)<10000,True)
    
    LF.Depth.LocalDepthHQ.numOfThreads = threads[3]
    LF.Depth.LocalDepthHQ.calcDepthHQ(self.test_lf,inner=0.7,outer=0.9)
    diff = np.sum(np.abs(self.test_lf.depth[2,2,:,:]-ref_disparity[:]))
    self.assertEqual(np.round(diff)<10000,True)
    
  def test_calcDepth_dimensionalityCheck(self):
    test_lf = createTestLF((5,5,448,512))
    LF.Depth.LocalDepth.calcDepth(test_lf,inner=0.7,outer=0.9)
    self.assertEqual(test_lf.depth.shape,(5,5,448,512))
    
    test_lf = createTestLF((7,5,512,512))
    LF.Depth.LocalDepth.calcDepth(test_lf,inner=0.7,outer=0.9)
    self.assertEqual(test_lf.depth.shape,(7,5,512,512))
    
    test_lf = createTestLF((5,7,448,512))
    LF.Depth.LocalDepth.calcDepth(test_lf,inner=0.7,outer=0.9)
    self.assertEqual(test_lf.depth.shape,(5,7,448,512))
    
  def test_calcDepthHQ_dimensionalityCheck(self):
    test_lf = createTestLF((5,5,448,512))
    LF.Depth.LocalDepthHQ.calcDepthHQ(test_lf,inner=0.7,outer=0.9)
    self.assertEqual(test_lf.depth.shape,(5,5,448,512))
    
    test_lf = createTestLF((7,5,512,512))
    LF.Depth.LocalDepthHQ.calcDepthHQ(test_lf,inner=0.7,outer=0.9)
    self.assertEqual(test_lf.depth.shape,(7,5,512,512))
    
    test_lf = createTestLF((5,7,448,512))
    LF.Depth.LocalDepthHQ.calcDepthHQ(test_lf,inner=0.7,outer=0.9)
    self.assertEqual(test_lf.depth.shape,(5,7,448,512))

