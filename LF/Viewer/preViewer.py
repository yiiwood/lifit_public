import sys
import numpy as np
import scipy.ndimage as nd
import qimage2ndarray

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtOpenGL import *
from OpenGL.GL import *
from OpenGL.GLU import *

from previewerUI import Ui_Previewer
from LF.Base.LightField import *
from LF.ImageProcessing.Improc import colorRange


 
class PreViewer(QWidget):
  def __init__(self, parent=None):
    QWidget.__init__(self, parent)
  
    
    self.saveResult = False
    
    self.ui = Ui_Previewer()
    self.ui.setupUi(self)
    self.connectSignals()
    self.setupScreens()
    
    
    
    
  def connectSignals(self):
    self.connect(self.ui.accept_preview, SIGNAL("clicked()"), self.accept)
    self.connect(self.ui.deny_preview, SIGNAL("clicked()"), self.deny)

    
  def setupScreens(self):
    self.ui.previewView.setViewport(QGLWidget())
    
    brush = QBrush()
    brush.setColor(QColor(0,0,0))
    brush.setStyle(Qt.SolidPattern)
    
    self.ui.previewView.setBackgroundBrush(brush)
    
    self.previewScene = QGraphicsScene()
    
    self.previewScene.clear()
    pixMap = QPixmap()
    self.previewSceneItem = QGraphicsPixmapItem(pixMap)
    self.previewSceneItem.setTransformationMode(Qt.SmoothTransformation)
    self.previewScene.addItem(self.previewSceneItem)
    self.ui.previewView.setScene(self.previewScene)
    self.ui.previewView.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
    self.ui.previewView.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
    self.ui.previewView.show()
    
    
  
  def render(self):
    self.previewScene.clear()
    self.previewSceneItem = QGraphicsPixmapItem(self.ndarrayToPixmap(self.currentImage))
    self.previewSceneItem.setTransformationMode(Qt.SmoothTransformation)
    self.previewScene.addItem(self.previewSceneItem)
    self.ui.previewView.setScene(self.previewScene)
    
    self.fitToScreen()
    
    
  def ndarrayToPixmap(self,array):
    if array.dtype != np.uint8:
      array = colorRange(array).astype(np.uint8)
    pic = qimage2ndarray.array2qimage(array)
    return QPixmap.fromImage(pic)
    
      
  def fitToScreen(self): 
    self.previewScene.setSceneRect(QRectF(0,0,self.shape[1],self.shape[0]))
    self.ui.previewView.fitInView(self.previewScene.sceneRect(), Qt.IgnoreAspectRatio)
    
    
    
    
  def setResult(self,result):
    self.shape = result.shape
    self.currentImage = result
    self.render()
    
    
  def accept(self):
    self.saveResult = True
    self.emit(SIGNAL('previewerClosed()'))
    self.close()
  
  
  def deny(self):
    self.saveResult = False
    self.emit(SIGNAL('previewerClosed()'))
    self.close()
