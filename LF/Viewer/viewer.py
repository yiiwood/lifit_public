#!/usr/bin/env python
import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from PyQt4.QtOpenGL import *
from OpenGL.GL import *
from OpenGL.GLU import *

from LF.Viewer.viewerUI import Ui_Viewer
from LF.Viewer.preViewer import *
from LF.Base.LightField import *
from LF.ImageProcessing.Improc import colorRange

try: from skimage.filter import tv_denoise
except: 
    print "seems you haven't installed skimage: on Ubuntu you can do this via($ pip install scikit-image)"
    sys.exit()
try: import numpy as np
except: 
    print "seems you haven't installed numpy: on Ubuntu you can do this via($ sudo apt-get install python-numpy)"
    sys.exit()
try: import scipy.ndimage as nd
except: 
    print "seems you haven't installed scipy: on Ubuntu you can do this via($ sudo apt-get install python-scipy)"
    sys.exit()
try: import qimage2ndarray
except: 
    print "seems you haven't installed qimage2ndarray: on Ubuntu you can do this via($ pip install qimage2ndarray)"
    sys.exit()
 
 
class TheViewer(QWidget):
  def __init__(self, parent=None,lf=None):
    QWidget.__init__(self, parent)
    
    self.view = None
    if lf is not None:
      if type(lf)==type("str"):
        print "type of argument is str:"
        try:
          self.lf = LightField().load(lf)
        except:
          sys.exit()
      else:
        try:
          if lf.__type__ == "LightField":
            print "type of argument is LightField:"
            self.lf = lf
        except:
          sys.exit()
    else:
      sys.exit()
      
      
    self.previewer = PreViewer()
    
    self.lf_array = self.lf.lf[:]
    
    
    self.ui = Ui_Viewer()
    self.ui.setupUi(self)
    self.setupWidgets()
    self.connectSignals()
    self.setupScreens()
    self.render()
    
    
    
    
    
    
  def setupWidgets(self):
    self.ui.verticalSlider.setRange(0,self.lf.yRes-1)
    self.ui.verticalSlider.setSliderPosition(self.lf.yRes/2)
    self.ui.horizontalSlider.setRange(0,self.lf.xRes-1)
    self.ui.horizontalSlider.setSliderPosition(self.lf.xRes/2)
    
    self.view = [self.lf.vRes/2,self.lf.hRes/2]
    self.verticalEpiPosition = self.lf.yRes/2
    self.horizontalEpiPosition = self.lf.xRes/2
    
    self.ui.horizontal_position.setText(str(self.horizontalEpiPosition))
    self.ui.vertical_position.setText(str(self.verticalEpiPosition))
    
    self.ui.horizontalViewSlider.setMaximum(self.lf.hRes-1)
    self.ui.horizontalViewSlider.setMinimum(0)
    self.ui.horizontalViewSlider.setValue(self.lf.hRes/2)
    self.ui.verticalViewSlider.setMaximum(self.lf.vRes-1)
    self.ui.verticalViewSlider.setMinimum(0)
    self.ui.verticalViewSlider.setValue(self.lf.vRes/2)

    if self.lf.depth is not None:
      self.ui.show_depth.setEnabled(True)
    else:
      self.ui.show_depth.setEnabled(False)
    if self.lf.gt is not None:
      self.ui.show_gt.setEnabled(True)
    else:
      self.ui.show_gt.setEnabled(False)
    if self.lf.depth is not None and self.lf.gt is not None:
      self.ui.show_diff.setEnabled(True)
    else:
      self.ui.show_diff.setEnabled(False)
      
    if self.lf.inner is None:
      outerScaleLookup = {"5":0.8,"7":0.9,"9":1.0,"11":1.1,"13":1.2,"15":1.3,"17":1.4}
      tmp = self.lf.vRes
      if self.lf.vRes > self.lf.hRes:
        tmp = self.lf.yRes
      self.ui.innerscale.setText(str(0.70))
      self.ui.innerscale.setText(str(outerScaleLookup[str(tmp)]))
    else:
      self.ui.innerscale.setText(str(self.lf.inner))
      self.ui.outerscale.setText(str(self.lf.outer))
    
    
  def connectSignals(self):
    self.connect(self.ui.verticalSlider,  SIGNAL('valueChanged(int)'), self.verticalSliderChange)
    self.connect(self.ui.horizontalSlider,  SIGNAL('valueChanged(int)'), self.horizontalSliderChange)
    self.connect(self.ui.horizontalViewSlider,  SIGNAL('valueChanged(int)'), self.horizontalViewChange)
    self.connect(self.ui.verticalViewSlider,  SIGNAL('valueChanged(int)'), self.verticalViewChange)
    self.connect(self.ui.show_lf, SIGNAL("clicked()"), self.showLF)
    self.connect(self.ui.show_depth, SIGNAL("clicked()"), self.showDepth)
    self.connect(self.ui.show_gt, SIGNAL("clicked()"), self.showGT)
    self.connect(self.ui.show_diff, SIGNAL("clicked()"), self.showDiff)
    self.connect(self.ui.calc_depth, SIGNAL("clicked()"), self.calcDepth)
    self.connect(self.ui.denoise, SIGNAL("clicked()"), self.denoise)
    
    self.connect(self.previewer, SIGNAL('previewerClosed()'), self.previewerClosed)


    
  def setupScreens(self):
    self.ui.mainView.setViewport(QGLWidget())
    self.ui.horizontalView.setViewport(QGLWidget())
    self.ui.verticalView.setViewport(QGLWidget())
    self.ui.pixelView.setViewport(QGLWidget())
    
    brush = QBrush()
    brush.setColor(QColor(0,0,0))
    brush.setStyle(Qt.SolidPattern)
    
    self.ui.mainView.setBackgroundBrush(brush)
    self.ui.horizontalView.setBackgroundBrush(brush)
    self.ui.verticalView.setBackgroundBrush(brush)
    self.ui.pixelView.setBackgroundBrush(brush)
    
    self.mainScene = QGraphicsScene()
    self.horizontalScene = QGraphicsScene()
    self.verticalScene = QGraphicsScene()
    self.pixelScene = QGraphicsScene()
    
    self.mainScene.clear()
    pixMap = QPixmap()
    self.mainSceneItem = QGraphicsPixmapItem(pixMap)
    self.mainSceneItem.setTransformationMode(Qt.SmoothTransformation)
    self.mainScene.addItem(self.mainSceneItem)
    self.ui.mainView.setScene(self.mainScene)
    self.ui.mainView.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
    self.ui.mainView.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
    self.ui.mainView.show()
    
    self.horizontalScene.clear()
    pixMap = QPixmap()
    self.horizontalSceneItem = QGraphicsPixmapItem(pixMap)
    #self.horizontalSceneItem.setTransformationMode(Qt.SmoothTransformation)
    self.horizontalScene.addItem(self.horizontalSceneItem)
    self.ui.horizontalView.setScene(self.horizontalScene)
    self.ui.horizontalView.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
    self.ui.horizontalView.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
    self.ui.horizontalView.show()

    self.verticalScene.clear()
    pixMap = QPixmap()
    self.verticalSceneItem = QGraphicsPixmapItem(pixMap)
    #self.verticalSceneItem.setTransformationMode(Qt.SmoothTransformation)
    self.verticalScene.addItem(self.verticalSceneItem)
    self.ui.verticalView.setScene(self.verticalScene)
    self.ui.verticalView.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
    self.ui.verticalView.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
    self.ui.verticalView.show()
    
    self.pixelScene.clear()
    pixMap = QPixmap()
    self.pixelSceneItem = QGraphicsPixmapItem(pixMap)
    #self.pixelSceneItem.setTransformationMode(Qt.SmoothTransformation)
    self.pixelScene.addItem(self.verticalSceneItem)
    self.ui.pixelView.setScene(self.pixelScene)
    self.ui.pixelView.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
    self.ui.pixelView.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
    self.ui.pixelView.show()
    
    self.showLF()
    
  
  def render(self):
    self.mainScene.clear()
    self.mainSceneItem = QGraphicsPixmapItem(self.currentImage)
    self.mainSceneItem.setTransformationMode(Qt.SmoothTransformation)
    self.mainScene.addItem(self.mainSceneItem)
    self.ui.mainView.setScene(self.mainScene)
    
    self.horizontalScene.clear()
    if self.ui.show_lf.isChecked():
      self.horizontalSceneItem = QGraphicsPixmapItem(self.getHorizontalEpi())
      self.horizontalSceneItem.setTransformationMode(Qt.SmoothTransformation)
      self.horizontalScene.addItem(self.horizontalSceneItem)
      self.ui.horizontalView.setScene(self.horizontalScene)#
    
    self.verticalScene.clear()
    if self.ui.show_lf.isChecked():
      self.verticalSceneItem = QGraphicsPixmapItem(self.getVerticalEpi())
      self.verticalSceneItem.setTransformationMode(Qt.SmoothTransformation)
      self.verticalScene.addItem(self.verticalSceneItem)
      self.ui.verticalView.setScene(self.verticalScene)
    
    self.pixelScene.clear()
    if self.ui.show_lf.isChecked():
      self.pixelSceneItem = QGraphicsPixmapItem(self.getPixelView())
      self.pixelSceneItem.setTransformationMode(Qt.SmoothTransformation)
      self.pixelScene.addItem(self.pixelSceneItem)
      self.ui.pixelView.setScene(self.pixelScene)
    
    if self.ui.show_lf.isChecked():
      self.drawLines()
    
    self.fitToScreen()
    
    
    
    
    
    

  
  
  def getHorizontalEpi(self):
    if self.view is not None:
      if self.lf.channels == 3:
        self.currentHorizontalEpi = self.lf_array[self.view[0],:,self.verticalEpiPosition,:,:]
      else:
        self.currentHorizontalEpi = self.lf_array[self.view[0],:,self.verticalEpiPosition,:,0]
        
    return self.ndarrayToPixmap(self.currentHorizontalEpi)
  
  
  def getVerticalEpi(self):
    if self.view is not None:      
      
      if self.lf.channels == 3:
        self.currentVerticalEpi = np.zeros((self.lf.yRes,self.lf.hRes,3),dtype=np.uint8)
        for i in range(3):
          tmp = self.lf_array[:,self.view[1],:,self.horizontalEpiPosition,i]
          self.currentVerticalEpi[:,:,i] = np.transpose(tmp)
      else:
        self.currentVerticalEpi = np.transpose(self.lf_array[:,self.view[1],:,self.horizontalEpiPosition,0])
       
    return self.ndarrayToPixmap(self.currentVerticalEpi)
  
  
  def getPixelView(self):
    if self.view is not None:
      
      tmp = np.zeros((self.lf.vRes,self.lf.hRes,self.lf.channels),dtype=np.uint8)
      self.currentPixelView = np.zeros((50,50,self.lf.channels),dtype=np.uint8)
      for c in range(self.lf.channels):
        for i in xrange(self.lf.vRes):
          for j in xrange(self.lf.hRes):
            tmp[i,j,:] = self.lf_array[i,j,self.verticalEpiPosition,self.horizontalEpiPosition,:]
        
      zoom = [50.0/self.lf.vRes,50.0/self.lf.hRes]
      self.currentPixelView[:,:,0] = nd.interpolation.zoom(tmp[:,:,0], zoom=zoom, order=0)
      self.currentPixelView[:,:,1] = nd.interpolation.zoom(tmp[:,:,1], zoom=zoom, order=0)
      self.currentPixelView[:,:,2] = nd.interpolation.zoom(tmp[:,:,2], zoom=zoom, order=0)
  
      return self.ndarrayToPixmap(self.currentPixelView)
  
  
  def drawLines(self):      
    pen = QPen()
    brush = QBrush()
    brush.setColor(QColor(255,0,0))
    brush.setStyle(Qt.SolidPattern)
    pen.setBrush(brush)
    self.mainScene.addLine(0,self.verticalEpiPosition,self.lf.xRes-1,self.verticalEpiPosition,pen)
    pen = QPen()
    brush = QBrush()
    brush.setColor(QColor(0,255,0))
    brush.setStyle(Qt.SolidPattern)
    pen.setBrush(brush)
    self.mainScene.addLine(self.horizontalEpiPosition,self.lf.yRes-1,self.horizontalEpiPosition,0,pen)
  
  
  def ndarrayToPixmap(self,array):
    if array.dtype != np.uint8:
      array = colorRange(array).astype(np.uint8)
    pic = qimage2ndarray.array2qimage(array)
    return QPixmap.fromImage(pic)
    
      
  def fitToScreen(self):
    self.mainScene.setSceneRect(QRectF(0,0,self.lf.xRes,self.lf.yRes))
      
    self.ui.mainView.fitInView(self.mainScene.sceneRect(), Qt.KeepAspectRatio)
    self.ui.verticalView.fitInView(self.verticalScene.sceneRect(), Qt.IgnoreAspectRatio)
    self.ui.horizontalView.fitInView(self.horizontalScene.sceneRect(), Qt.IgnoreAspectRatio)
    self.ui.pixelView.fitInView(self.pixelScene.sceneRect(), Qt.IgnoreAspectRatio)
    
    




  def resizeEvent(self, event):
    self.fitToScreen()
    
    
  def showEvent(self, event):
    self.fitToScreen()
    
    
  def verticalSliderChange(self,value):
    self.verticalEpiPosition = self.lf.yRes-1-value
    self.ui.vertical_position.setText(str(self.lf.yRes-1-value))
    self.render()
    
    
  def horizontalSliderChange(self,value):
    self.horizontalEpiPosition = value
    self.ui.horizontal_position.setText(str(value))
    self.render()
    
    
  def horizontalViewChange(self,value):
    self.view[1] = value
    self.showLF()
  
  
  def verticalViewChange(self,value):
    self.view[0] = value
    self.showLF()
  
    
  def showLF(self):
    if self.ui.show_lf.isChecked():
      self.ui.lamda.setEnabled(False)
      self.ui.iter.setEnabled(False)
      self.ui.denoise.setEnabled(False)
      self.ui.innerscale.setEnabled(True)
      self.ui.outerscale.setEnabled(True)
      self.ui.calc_depth.setEnabled(True)
      self.ui.horizontalViewSlider.setEnabled(True)
      self.ui.verticalViewSlider.setEnabled(True)
      self.ui.horizontalSlider.setEnabled(True)
      self.ui.verticalSlider.setEnabled(True)
      if self.lf.channels == 3:
        self.currentImage = self.lf_array[self.view[0],self.view[1],:,:,:]
      else:
        self.currentImage = self.lf_array[self.view[0],self.view[1],:,:,0]
        
      self.currentImage = self.ndarrayToPixmap(self.currentImage)
      self.render()
      
      
  def showDepth(self):
    if self.ui.show_depth.isChecked():
      self.ui.lamda.setEnabled(True)
      self.ui.iter.setEnabled(True)
      self.ui.denoise.setEnabled(True)
      self.ui.innerscale.setEnabled(True)
      self.ui.outerscale.setEnabled(True)
      self.ui.calc_depth.setEnabled(True)
      self.ui.horizontalViewSlider.setEnabled(False)
      self.ui.verticalViewSlider.setEnabled(False)
      self.ui.horizontalSlider.setEnabled(False)
      self.ui.verticalSlider.setEnabled(False)
      
      tmp = self.lf.depth[:]
      
      if len(tmp.shape) != 2:
        try:
          print "try to slice view:",self.lf.vRes/2,self.lf.hRes/2
          tmp = self.lf.depth[self.lf.vRes/2,self.lf.hRes/2,:,:]
          print "tmp now:",tmp.shape
        except:
          sys.exit()
      
      self.currentImage = self.ndarrayToPixmap(tmp)
      
      self.render()
      
  
  def showGT(self):
    if self.ui.show_gt.isChecked():
      self.ui.lamda.setEnabled(False)
      self.ui.iter.setEnabled(False)
      self.ui.denoise.setEnabled(False)
      self.ui.innerscale.setEnabled(False)
      self.ui.outerscale.setEnabled(False)
      self.ui.calc_depth.setEnabled(False)
      self.ui.horizontalViewSlider.setEnabled(False)
      self.ui.verticalViewSlider.setEnabled(False)
      self.ui.horizontalSlider.setEnabled(False)
      self.ui.verticalSlider.setEnabled(False)
      from LF.Depth.Convert import depthToDisparity
      
      try:
        tmp = depthToDisparity(self.lf,self.lf.gt[self.lf.vRes/2,self.lf.hRes/2,:,:])
      except:
        try:
          tmp = depthToDisparity(self.lf,self.lf.gt[:,:])
        except:
          tmp = np.zeros((self.lf.yRes,self.lf.xRes),dtype=np.float32)
          print "WARNING, error in showGT: Slice GT error"
      
      
      self.currentImage = self.ndarrayToPixmap(tmp)      
      
      self.render()
      
      
  def showDiff(self):
    if self.ui.show_diff.isChecked():
      self.ui.lamda.setEnabled(False)
      self.ui.iter.setEnabled(False)
      self.ui.denoise.setEnabled(False)
      self.ui.innerscale.setEnabled(False)
      self.ui.outerscale.setEnabled(False)
      self.ui.calc_depth.setEnabled(False)
      self.ui.horizontalViewSlider.setEnabled(False)
      self.ui.verticalViewSlider.setEnabled(False)
      self.ui.horizontalSlider.setEnabled(False)
      self.ui.verticalSlider.setEnabled(False)
      from LF.Depth.Convert import depthToDisparity
      from LF.ImageProcessing import Improc as improc

      try:
        tmp_gt = depthToDisparity(self.lf,self.lf.gt[self.lf.vRes/2,self.lf.hRes/2,:,:])
      except:
        try:
          tmp_gt = depthToDisparity(self.lf,self.lf.gt[:,:])
        except:
          tmp_gt = np.zeros((self.lf.yRes,self.lf.xRes),dtype=np.float32)
          print "WARNING, error in showDiff: Slice GT error"
          
      try:
        tmp_d = self.lf.depth[self.lf.vRes/2,self.lf.hRes/2,:,:]
      except:
        try:
          tmp_d = self.lf.depth[:,:]
        except:
          tmp_d = np.zeros((self.lf.yRes,self.lf.xRes),dtype=np.float32)
          print "WARNING, error in showDiff: Slice depth error"

      fig = np.abs(np.abs(tmp_gt[:]-tmp_d[:]))
      cmap = plt.cm.jet(np.arange(256))
      np.place(fig,fig>3,3.0)
      fig = fig[:]/3.0*255

      im = improc.colormap(fig.astype(np.uint8), cmap.astype(np.float32))
          
      self.currentImage = self.ndarrayToPixmap(im[:])
      self.render()
    


      
    
    
    

  def calcDepth(self):
    from LF.Depth.LocalDepthHQ import calcDepthHQ as calcDepth
    self.ui.show_depth.setEnabled(True)
    if self.lf.gt is not None:
      self.ui.show_gt.setEnabled(True)
      self.ui.show_diff.setEnabled(True)
      
    self.params = [float(self.ui.innerscale.text()),float(self.ui.outerscale.text())]
    calcDepth(self.lf,inner=self.params[0],outer=self.params[1])
    self.ui.show_depth.setChecked(True)
    self.showDepth()
    
    
  def denoise(self):
    self.params = [float(self.ui.lamda.text()),float(self.ui.iter.text())]
    tmp = self.lf.depth[:]
      
    if len(tmp.shape) != 2:
      try:
        print "try to slice view:",self.lf.vRes/2,self.lf.hRes/2
        tmp = self.lf.depth[self.lf.vRes/2,self.lf.hRes/2,:,:]
        print "tmp now:",tmp.shape
      except:
        sys.exit()

    denoised = tv_denoise(tmp,weight=self.params[0],n_iter_max=self.params[1])
    #denoised = tv_regularizer(tmp,self.params[0],self.params[1],1)
    self.previewer.setResult(denoised)
    self.previewer.show()
    
    
  def previewerClosed(self):
    if self.previewer.saveResult:
      self.lf.depth = self.previewer.currentImage
      self.showDepth()
      #saveLF(self.lf)
      
    
    
    
    
    
    
def show(lfin=None):
  """
  @author: Sven Wanner
  @summary: Load Light Field in Viewer
  @param lfin: filename or lf instance, default:None
  """
  app = QApplication(sys.argv)
  myapp = TheViewer(lf=lfin)
  myapp.show()
  sys.exit(app.exec_())
    
   
if __name__ == "__main__":
  if len(sys.argv) == 2:
    app = QApplication(sys.argv)
    myapp = TheViewer(lf=sys.argv[1])
    myapp.show()
    sys.exit(app.exec_())
  else:
    print "Need a Light Field as argument, nothing more and nothing less!"
