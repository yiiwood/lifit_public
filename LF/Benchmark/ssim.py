"""
This module computes the Structured Similarity Image Metric (SSIM)

Created on 21 nov. 2011
@author: Antoine Vacavant, ISIT lab, antoine.vacavant@iut.u-clermont1.fr, http://isit.u-clermont1.fr/~anvacava

Modified by Christopher Godfrey, on 17 July 2012 (lines 32-34)
Modified by Jeff Terrace, starting 29 August 2012
Modified by Sven Wanner, 25 January 2013
"""

import sys

try: import vigra
except: 
    print "seems you haven't installed vigra: visit http://hci.iwr.uni-heidelberg.de/vigra/ for more information"
    sys.exit()
try: import numpy
except: 
    print "seems you haven't installed numpy: on Ubuntu you can do this via($ sudo apt-get install python-numpy)"
    sys.exit()



def compute_ssim(im1, im2, L=255, gaussian_kernel_sigma=1.5):
    """
    @autor: Sven Wanner
    @bief:function to compute SSIM
    @param im1:<ndarray> rgba,rgb or bw image
    @param im2:<ndarray> rgba,rgb or bw image
    @param L:<float>[255] dynamic range of the image
    @param gaussian_kernel_sigma:<float>[1.5] smoothing scale
    @return: SSIM float value
    """
    
    assert im1.shape == im2.shape
    
    # convert the images to grayscale
    img_alpha_1 = None
    img_alpha_2 = None
    if len(im1.shape) == 2:
      img_mat_1 = im1
      img_mat_2 = im2
    elif len(im1.shape) == 3:
      img_mat_1 = 0.3*im1[:,:,0]+0.11*im1[:,:,1]+0.59*im1[:,:,2]
      img_mat_2 = 0.3*im2[:,:,0]+0.11*im2[:,:,1]+0.59*im2[:,:,2]
    elif len(im1.shape) == 4:
      img_mat_1 = 0.3*im1[:,:,0]+0.11*im1[:,:,1]+0.59*im1[:,:,2]
      img_mat_2 = 0.3*im2[:,:,0]+0.11*im2[:,:,1]+0.59*im2[:,:,2]
      img_alpha_1 = im1[:,:,3]
      img_alpha_2 = im2[:,:,3]
    else:
      raise Exception("Unknown image type!")
    
    # don't count pixels where both images are both fully transparent
    if img_alpha_1 is not None and img_alpha_2 is not None:
      img_mat_1[img_alpha_1 == 255] = 0
      img_mat_2[img_alpha_2 == 255] = 0
    
    #Squares of input matrices
    img_mat_1_sq = img_mat_1 ** 2
    img_mat_2_sq = img_mat_2 ** 2
    img_mat_12 = img_mat_1 * img_mat_2
    
    #Means obtained by Gaussian filtering of inputs
    img_mat_mu_1 = vigra.filters.gaussianSmoothing(img_mat_1.astype(numpy.float32), gaussian_kernel_sigma)
    img_mat_mu_2 = vigra.filters.gaussianSmoothing(img_mat_2.astype(numpy.float32), gaussian_kernel_sigma)
    
    #Squares of means
    img_mat_mu_1_sq = img_mat_mu_1 ** 2
    img_mat_mu_2_sq = img_mat_mu_2 ** 2
    img_mat_mu_12 = img_mat_mu_1 * img_mat_mu_2
    
    img_mat_sigma_1_sq = vigra.filters.gaussianSmoothing(img_mat_1_sq.astype(numpy.float32), gaussian_kernel_sigma)
    img_mat_sigma_2_sq = vigra.filters.gaussianSmoothing(img_mat_2_sq.astype(numpy.float32), gaussian_kernel_sigma)
    
    #Covariance
    img_mat_sigma_12 = vigra.filters.gaussianSmoothing(img_mat_12.astype(numpy.float32), gaussian_kernel_sigma)
    
    #Centered squares of variances
    img_mat_sigma_1_sq = img_mat_sigma_1_sq - img_mat_mu_1_sq
    img_mat_sigma_2_sq = img_mat_sigma_2_sq - img_mat_mu_2_sq
    img_mat_sigma_12 = img_mat_sigma_12 - img_mat_mu_12
    
    #set k1,k2 & c1,c2 to depend on L (width of color map)
    l = L
    k_1 = 0.01
    c_1 = (k_1 * l) ** 2
    k_2 = 0.03
    c_2 = (k_2 * l) ** 2
    
    #Numerator of SSIM
    num_ssim = (2 * img_mat_mu_12 + c_1) * (2 * img_mat_sigma_12 + c_2)
    
    #Denominator of SSIM
    den_ssim = ((img_mat_mu_1_sq + img_mat_mu_2_sq + c_1) * (img_mat_sigma_1_sq + img_mat_sigma_2_sq + c_2))
    
    #SSIM
    ssim_map = num_ssim / den_ssim
    index = numpy.average(ssim_map)

    return index