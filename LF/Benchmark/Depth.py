import LF
import os 
import sys
from LF.settings import *
import LF.Benchmark.ssim as ssim
from LF.Helpers import ensure_dir
from LF.Depth.LocalDepth import calcDepth
from LF.Depth.LocalDepthHQ import calcDepthHQ
from LF.Depth.Convert import disparityToDepth, depthToDisparity

try: 
    from scipy.misc import imsave
except: 
    print "seems you haven't installed scipy: on Ubuntu you can do this via($ sudo apt-get install python-scipy)"
    exit()
try: import numpy as np
except: 
    print "seems you haven't installed numpy: on Ubuntu you can do this via($ sudo apt-get install python-numpy)"
    sys.exit()


def benchmark_localDepthAlgos(lf,deltas=[0.001,0.01],inner=0.9,outer=1.1,minLabel=-2.5,maxLabel=2.5):
  
  print "\nevaluate LocalDepth:"
  calcDepth(lf,inner=inner,outer=outer,minLabel=minLabel,maxLabel=maxLabel)
  benchmark_localDepth_relative(lf,deltas)
  benchmark_localDepth_meanSquare(lf)
  benchmark_localDepth_ssim(lf)
  print "\nevaluate LocalDepthHQ:"
  calcDepthHQ(lf,inner=inner,outer=outer,minLabel=minLabel,maxLabel=maxLabel)
  benchmark_localDepth_relative(lf,deltas)
  benchmark_localDepth_meanSquare(lf)
  benchmark_localDepth_ssim(lf)

  
  
  

def benchmark_localDepth_ssim(lf):
  gt = depthToDisparity(lf,np.copy(lf.gt[:]))
  
  results = []
  cv_index = 0
  n=0
  for v in range(lf.vRes):
    for h in range(lf.hRes):
      results.append(ssim.compute_ssim(lf.depth[v,h,:,:],gt[v,h,:,:],L=5))
      if v==lf.vRes/2 and h==lf.hRes/2:
        cv_index = n
      n+=1
      
  print "\nssim measure:"
  print "  cv results:",results[cv_index]
  print "  tot results:",np.mean(results)
  return [results[cv_index],np.mean(results)]
      
  

def benchmark_localDepth_meanSquare(lf):
  gt = depthToDisparity(lf,np.copy(lf.gt[:]))
  
  diff = (np.copy(lf.depth[:])-gt[:])**2
  
  cv_result = np.mean(diff[lf.vRes/2,lf.hRes/2,:,:])
  tot_result = np.mean(diff[:])
  
  if DEBUG>0:
    print "\nsmean square measure:"
    print "  cv results:",cv_result
    print "  tot results:",tot_result
  return [cv_result,tot_result]

  

def benchmark_localDepth_relative(lf,deltas=[0.001,0.01],saveResults=None,name=""):
  ndev = np.zeros((lf.depth.shape),dtype=np.float32)
  cv_results = {}
  tot_results = {}
  for v in range(lf.vRes):
    for h in range(lf.hRes):
      ndev[v,h,:,:] = np.abs(disparityToDepth(lf,lf.depth[v,h,:,:])-lf.gt[v,h,:,:])/lf.gt[v,h,:,:]
    
      if v==lf.vRes/2 and h==lf.hRes/2:
        if saveResults is not None:
          d = ensure_dir(saveResults)
          if not d.endswith("/"):
            d+="/"
          imsave(d+name+"_normed_diff.png",ndev[v,h,:,:])
      
        for delta in deltas:
          counts = np.where(ndev[v,h,:,:]<delta)[0].shape[0]
          cv_results[str(delta)] = counts*(100.0/(lf.yRes*lf.xRes))
          
  for delta in deltas:
    counts = np.where(ndev[:]<delta)[0].shape[0]
    tot_results[str(delta)] = counts*(100.0/(lf.yRes*lf.xRes*lf.vRes*lf.hRes))
    
  if DEBUG>0:
    print "\nrelative measure:"
    for delta in deltas:
      print "  cv results:"
      print "   ",delta,":",cv_results[str(delta)]
    for delta in deltas:
      print "  total results:"
      print "   ",delta,":",tot_results[str(delta)]
    
  return [cv_results,tot_results]
          
          
          
def benchmark_localDepth_gridSearch(lf,delta=[0.001],scales=[0.6,3.0],step=0.1,save=None,name="benchmark",algo="HQ"):
  
  if type(lf)==type(""):
    lf = LF.LightField().load(lf)
  
  results = []
  steps = int(np.ceil((scales[1]-scales[0])/step)+1)
  
  for i in range(steps):
    inner = scales[0]+i*step
    for o in range(steps):
      outer = scales[0]+o*step
      
      if algo == "HQ":
        LF.calcDepthHQ(lf,inner,outer)
      elif algo == "LQ":
        LF.calcDepth(lf,inner,outer)
        
      results.append([[inner,outer],benchmark_localDepth_relative(lf,deltas=delta),benchmark_localDepth_meanSquare(lf),benchmark_localDepth_ssim(lf)])
  
  scales = []
  mse = []
  ssim = []
  re =[]
  for res in results:
    scales.append(res[0])
    ssim.append(res[3][0])
    mse.append(res[2][0])
    re.append(res[1][0][res[1][0].keys()[0]])
    
  min_mse_index = np.argmin(mse)
  max_re_index = np.argmax(re)
  max_ssim_index = np.argmax(ssim)
  
  if DEBUG>0:
    print "\n--------  RESULTS CV   ------------"
    print "relative error max:",re[max_re_index],"for scales:",scales[max_re_index]
    print "mean suare error min:",mse[min_mse_index],"for scales:",scales[min_mse_index]
    print "ssim error min:",ssim[max_ssim_index],"for scales:",scales[max_ssim_index]
 
  if save is not None and type(save)==type(""):
    save = ensure_dir(save)
    if not save.endswith("/"):
      save+="/"
    filename = save+name+".bm"
    if os.path.exists(filename):
      file = open(filename, 'w+')
    else:
      file = open(filename, "w") 
      
    
    for res in results:
      file.write("inner:"+str(res[0][0])+"  outer:"+str(res[0][1])+"\n")
      file.write(res[1][0].keys()[0]+" : "+str(res[1][0][res[1][0].keys()[0]])+"\n")
      file.write("mean square:"+str(res[2][0])+"\n")
      file.write("ssim:"+str(res[3][0]))
      file.write("\n#\n")
    file.close()
    
  parseBenchmarkOutput(filename)
    
      
      
def parseBenchmarkOutput(path):
  file = open(path,'r')

  measurements = []
  measure = {}
  
  while 1:
    line = file.readline()
    if not line:
      break
    if line.find('#') != -1:
      measurements.append(measure)
      measure = {}
            
    if line.find("inner") != -1:
      measure['scales'] = line
    if line.find("0.001") != -1:
      measure["re"] = float(line[7:-1])
    if line.find("mean") != -1:
      measure["mse"] = float(line[12:-1])
    if line.find("ssim") != -1:
      measure["ssim"] = float(line[5:-1])
      
      
  file.close()
  
  max_ssim = measurements[0]["ssim"]
  min_mse = measurements[0]["mse"]
  max_re = measurements[0]["re"]
  index_ssim = 0
  index_mse = 0
  index_re = 0
  
  for n,measure in enumerate(measurements):
    if n==0:
      pass
    if measure["ssim"] > max_ssim:
      max_ssim = measure["ssim"]
      index_ssim = n
    if measure["mse"] < min_mse:
      min_mse = measure["mse"]
      index_mse = n
    if measure["re"] > max_re:
      max_re = measure["re"]
      index_re = n
        
  
  s1 = "min mse: "+str(min_mse)+" at "+str(measurements[index_mse]['scales'])+"\n"
  s2 = "max re: "+str(max_re)+" at "+str(measurements[index_re]['scales'])+"\n"
  s3 = "max ssim: "+str(max_ssim)+" at "+str(measurements[index_ssim]['scales'])+"\n"
  
  print "\n"
  print s1
  print s2
  print s3
  
  f = open(path+".final","w+")
  f.write(s1)
  f.write(s2)
  f.write(s3)
  
      
      