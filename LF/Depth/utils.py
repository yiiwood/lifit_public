import sys
import vigra
import traceback
import numpy as np  

import multiprocessing

CPUS = multiprocessing.cpu_count()
numOfThreads = None
if CPUS >= 8:
  numOfThreads = 8
elif CPUS >= 4:
  numOfThreads = 4
elif CPUS >= 2:
  numOfThreads = 2
else:
  numOfThreads = 1

OVERLAP = 5

def mergeResultPieces(pieces,overlap=OVERLAP):
  """
  @author: Sven Wanner
  @brief: merges light field pieces together again
  @param pieces:list<ndarrays> light field pieces 
  @param overlap:<int>[20] number of overlap pixels, in case of visible tiles increase
  @return lf_array:<ndarray> merges light field
  """
  if type(pieces) != type([]):
    raise Exception("mergeLightField Argument Error!")
  
  if len(pieces) == 1:
    return pieces[0]
  
  if len(pieces) == 2:
    yRes = 2*pieces[0].shape[2]-2*overlap
    xRes = pieces[0].shape[3]
    lf_array = np.zeros((pieces[0].shape[0],pieces[0].shape[1],yRes,xRes,pieces[0].shape[4]),dtype=pieces[0].dtype)

    lf_array[:,:,0:yRes/2,:,:] = pieces[0][:,:,:-overlap,:]
    lf_array[:,:,yRes/2:,:,:] = pieces[1][:,:,overlap:,:]
    return lf_array
  
  if len(pieces) == 4:
    yRes = 2*pieces[0].shape[2]-2*overlap
    xRes = 2*pieces[0].shape[3]-2*overlap
    lf_array = np.zeros((pieces[0].shape[0],pieces[0].shape[1],yRes,xRes,pieces[0].shape[4]),dtype=pieces[0].dtype)

    lf_array[:,:,0:yRes/2,0:xRes/2,:] = pieces[0][:,:,:-overlap,:-overlap,:]
    lf_array[:,:,0:yRes/2,xRes/2:,:] = pieces[1][:,:,:-overlap,overlap:,:]
    lf_array[:,:,yRes/2:,0:xRes/2,:] = pieces[2][:,:,overlap:,:-overlap,:]
    lf_array[:,:,yRes/2:,xRes/2:,:] = pieces[3][:,:,overlap:,overlap:,:]
    return lf_array
      

      

def splitLightField(lf_array,overlap=OVERLAP,numOfThreads=numOfThreads):
  """
  @author: Sven Wanner
  @brief: splits the light field array into pieces according to number of threads
  @param lf_array:<ndarray> 
  @param overlap:<int>[20] number of overlap pixels, in case of visible tiles increase
  @return list<ndarrays>: light field pieces
  """
  spatialRes = [lf_array.shape[2],lf_array.shape[3]]
  splitVal = numOfThreads/2
  if numOfThreads == 1 or numOfThreads is None:
    splitVal = 1
    numOfThreads = 1
    
  if splitVal == 1:
    return [lf_array]
  elif splitVal == 2:
    return [lf_array[:,:,0:spatialRes[0]/2+overlap,:,:],lf_array[:,:,spatialRes[0]/2-overlap:,:,:]]
  elif splitVal == 4:
    return [lf_array[:,:,0:spatialRes[0]/2+overlap,0:spatialRes[1]/2+overlap,:],
            lf_array[:,:,0:spatialRes[0]/2+overlap,spatialRes[1]/2-overlap:,:],
            lf_array[:,:,spatialRes[0]/2-overlap:,0:spatialRes[1]/2+overlap,:],
            lf_array[:,:,spatialRes[0]/2-overlap:,spatialRes[1]/2-overlap:,:]
            ]


def getSubspace(lf_array,v=None,h=None):
  """
  @author: Sven Wanner
  @brief: Returns h/v subspaces of a 4D light field
  @param lf:<ndarray> 
  @param v:<int>[None] angular in v direction
  @param h:<int>[None] angular in u direction
  @return ndarray: h or v subspace
  """
  
  if h is not None and v is None:
    try:
      return lf_array[h,:,:,:,:].astype(np.float32)
    except:
      raise Exception("subspace selection error!")
      traceback.print_exc()
      sys.exit()
  elif v is not None and h is None:
    try:
      return lf_array[:,v,:,:,:].astype(np.float32)
    except:
      raise Exception("subspace selection error!")
      traceback.print_exc()
      sys.exit()


def mergeLFPieces(pieces,overlap):
  """
  @author: Sven Wanner
  @brief: merges light field pieces together again
  @param pieces:list<ndarrays> light field pieces 
  @param overlap:<int>[20] number of overlap pixels, in case of visible tiles increase
  @return lf_array:<ndarray> merges light field
  """
  if type(pieces) != type([]):
    raise Exception("mergeLightField Argument Error!")
  
  if len(pieces) == 1:
    return pieces[0]
  
  if len(pieces) == 2:
    yRes = 2*pieces[0].shape[2]-2*overlap
    xRes = pieces[0].shape[3]
    lf_array = np.zeros((pieces[0].shape[0],pieces[0].shape[1],yRes,xRes,pieces[0].shape[4]),dtype=np.uint8)

    lf_array[:,:,0:yRes/2,:,:] = pieces[0][:,:,:-overlap]
    lf_array[:,:,yRes/2:,:,:] = pieces[1][:,:,overlap:]
    return lf_array
  
  if len(pieces) == 4:
    yRes = 2*pieces[0].shape[2]-2*overlap
    xRes = 2*pieces[1].shape[3]-2*overlap
    lf_array = np.zeros((pieces[0].shape[0],pieces[0].shape[1],yRes,xRes,pieces[0].shape[4]),dtype=np.uint8)

    lf_array[:,:,0:yRes/2,0:xRes/2,:] = pieces[0][:,:,:-overlap,:-overlap]
    lf_array[:,:,0:yRes/2,xRes/2:,:] = pieces[1][:,:,:-overlap,overlap:]
    lf_array[:,:,yRes/2:,0:xRes/2,:] = pieces[2][:,:,overlap:,:-overlap]
    lf_array[:,:,yRes/2:,xRes/2:,:] = pieces[3][:,:,overlap:,overlap:]
    return lf_array
  
  
  
def channelReduction(img,reduction_type="rgb_bw"):
  """
  @brief: reduce colorspace according to type parameter
  @param img:<3d ndarray> input rgb image
  @param reduction_type:<str> type flag
  @param return: <2d ndarray> output bw image
  """
  if len(img.shape) == 2:
    return img
  elif len(img.shape) == 3 and img.shape[2] == 1:
    return img[:,:,0]
  elif len(img.shape) == 3 and img.shape[2] == 3:
    if reduction_type == "rgb_bw": 
      return 0.3*img[:,:,0]+0.11*img[:,:,1]+0.59*img[:,:,2]
  else:
    raise Exception("invalid image format for channel reduction!")