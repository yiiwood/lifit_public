import sys
import traceback
import threading
from utils import *
from time import time
from LF.settings import *
from types import StringType
import structureTensor as st
from Convert import getDisparityGT
from LF.ImageProcessing.UI import *
from Convert import mergeDepthChannels

try: import vigra
except: 
    print "seems you haven't installed vigra: visit http://hci.iwr.uni-heidelberg.de/vigra/ for more information"
    sys.exit()
try: import numpy as np
except: 
    print "seems you haven't installed numpy: on Ubuntu you can do this via($ sudo apt-get install python-numpy)"
    sys.exit()
try: from scipy.misc import imsave
except: 
    print "seems you haven't installed scipy: on Ubuntu you can do this via($ sudo apt-get install python-scipy)"
    sys.exit()


import multiprocessing
CPUS = multiprocessing.cpu_count()
numOfThreads = None
if CPUS >= 8:
  numOfThreads = 8
elif CPUS >= 4:
  numOfThreads = 4
elif CPUS >= 2:
  numOfThreads = 2
else:
  numOfThreads = 1

def calcDepthHQ(lf,inner=0.7,outer=0.9,maxLabel=3,minLabel=-3,overlap=OVERLAP,saveChannels=False,multithread=True,useGT=True):
  """
  @author: Sven Wanner
  @brief: calculates disparity from light field instance
  @param lf:<object> LightField instance
  @param overlap:<int>[5] number of overlap pixels, in case of visible tiles increase
  @param threading:<bool>[True] if disabled number of CPUs is ignored for computation
  @return lf:<object> LightField instance
  """
  
  if useGT:
    if lf.gt is not None:
      tmp = getDisparityGT(lf,view=[lf.vRes/2,lf.hRes/2])
      maxLabel = np.amax(tmp)
      minLabel = np.amin(tmp)
  
  t0=time()
  if overlap <= 0:
    overlap = OVERLAP
  params = {"inner":inner,
            "outer":outer,
            "maxLabel":maxLabel,
            "minLabel":minLabel
            }
  
  if DEBUG>0:
    print "\n### calcDepth HQ ###"
    print "  number of threads:",numOfThreads
    if multithread: print "  threading: ENABLED"
    else: print "  threading: DISABLED"
    print "  overlap:",overlap
    print "  inner scale:",params["inner"]
    print "  outer scale:",params["outer"]
    print "  maxLabel:",params["maxLabel"]
    print "  minLabel:",params["minLabel"]
    print "  ",
    
  result_h = np.zeros((lf.vRes,lf.hRes,lf.yRes,lf.xRes,2),dtype=np.float32)
  result_v = np.zeros_like(result_h)
    
  #calc disparities and coherences
  if not multithread:
    if lf.vRes > 1:
      labelDirection(lf.lf,result_v,params,'v')
    if lf.hRes > 1:
      labelDirection(lf.lf,result_h,params,'h')
  else:
    pieces = splitLightField(lf.lf,overlap=overlap,numOfThreads=numOfThreads)
    results = []
    for piece in pieces:
      results.append([
                      np.zeros((piece.shape[0],piece.shape[1],piece.shape[2],piece.shape[3],2),dtype=np.float32),
                      np.zeros((piece.shape[0],piece.shape[1],piece.shape[2],piece.shape[3],2),dtype=np.float32)
                      ])
    for n,piece in enumerate(pieces):
      threads = []
      if lf.vRes > 1:
        t = threading.Thread(target=labelDirection,args=(piece,results[n][0],params,'v',))
        threads.append(t)
        t.start()
      if lf.hRes > 1:
        t = threading.Thread(target=labelDirection,args=(piece,results[n][1],params,'h',))
        threads.append(t)
        t.start()
    for t in threads:
      t.join()
  
  
    #merge pieces together
    if DEBUG>0: print "\n  merge",len(results),"pieces"
    h_pieces = []
    v_pieces = []
    for res in results:
      v_pieces.append(res[0][:])
      h_pieces.append(res[1][:])
      
    result_v[:,:,:,:,:] = mergeResultPieces(v_pieces,overlap=overlap)
    result_h[:,:,:,:,:] = mergeResultPieces(h_pieces,overlap=overlap)
  
  #invert v channel!
  if lf.vRes > 1:
    result_v[:,:,:,:,0]*=-1 
    
  #save single channel results [optional]
  if saveChannels is not None:
    from LF.Helpers import ensure_dir
    loc = ensure_dir(saveChannels)
    if loc is not None:
      from LF.Base.IO import saveDisparityChannels
      try:
        saveDisparityChannels(result_v,result_h,loc)
      except:
        print "Warning, saving disparity channels failed!"
        traceback.print_exc()
       
  #do coherence merge 
  labeled, coherence = mergeDepthChannels(result_h[:,:,:,:,1],result_v[:,:,:,:,1],result_h[:,:,:,:,0],result_v[:,:,:,:,0]) 
  lf.depth = labeled[:]
  
  if DEBUG>0: print "\n  Duration:",time()-t0,"sec"
  if DEBUG>0: print "  Done"
  
  
      
def labelDirection(lf_array,result,params,direction):
  """
  @brief: labels depth regions on epipolar plane space of an 4d input lightfield 
  @param lf_array:<4d ndarray> input light field array
  @param result:<5d ndarray> holds disparity and coherence results result[:,:,:,:,0]:disparity result[:,:,:,:,1]:coherence 
  @param params:<dictionary> parameter: {"inner","outer","maxLabel","minLabel"}
  @param direction:<char> label direction 'v','h'
  """
  
  if direction == 'h':
    status = lf_array.shape[0]
    angles = lf_array.shape[0]
  elif direction == 'v':
    status = lf_array.shape[1]
    angles = lf_array.shape[1]
  
  for angle in range(angles):
    if DEBUG>0: print ".",
    status -= 1

    if direction == 'h':
      subLF = getSubspace(lf_array,h=angle)
    elif direction == 'v':
      subLF = getSubspace(lf_array,v=angle)
      
    d=0
    if direction == 'h':
      d=1
      
    try:
      slope = np.zeros(subLF.shape[0:-1],dtype=np.float32)
      coh = np.zeros(subLF.shape[0:-1],dtype=np.float32)
      st.calcSubLFDisparityAndCoherence_RGB(subLF.astype(np.float32),d,params["inner"],params["outer"],params["minLabel"],params["maxLabel"],coh,slope)

    except:
      raise Exception("getDirections error!")
      traceback.print_exc()
      sys.exit()
         
    if direction == 'h':
      result[angle,:,:,:,0] = slope[:]
      result[angle,:,:,:,1] = coh[:]
    elif direction == 'v':     
      result[:,angle,:,:,0] = slope[:,:]
      result[:,angle,:,:,1] = coh[:,:]