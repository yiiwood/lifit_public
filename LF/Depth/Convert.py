import coherenceMerge as cm

try: import numpy as np
except: 
    print "seems you haven't installed numpy: on Ubuntu you can do this via($ sudo apt-get install python-numpy)"
    exit()


def getDisparityGT(lf,view=None):
  
  if view is None:
    gt = np.zeros_like(lf.gt[:])
    for i in range(lf.vRes):
      for j in range(lf.hRes):
        gt[i,j,:] = depthToDisparity(lf,lf.gt[i,j,:])
  else:
    gt = np.zeros_like(lf.gt[view[0],view[1],:,:])
    gt[:,:] = depthToDisparity(lf,lf.gt[view[0],view[1],:,:])
    
  return gt



def depthToDisparity(lf,depth,baseLine=None,camDistance=None,camAngle=None,xRes=None):
  """
  @author: Sven Wanner
  @brief: generates a 2d depth image from a 2d disparity image
  @param disp: <2d ndarray> disparity image
  @param baseLine: <float> camera base line in blender units
  @param camDistance: <float> distance of blender cam to scene origin
  @param camAngle: <float> blender cam opening angle in degrees
  @return: 2d ndarray
  """
  
  if baseLine is None:
    baseLine = lf.dH
  if camDistance is None:
    camDistance = lf.camDistance
  if camAngle is None:
    camAngle = lf.focalLength
  if xRes is None:
    xRes = lf.xRes

  shift = 1.0/(2.0*camDistance*np.tan(camAngle/2.0))*baseLine
  #calc disparities
  disp = baseLine*xRes/(2.0*np.tan(camAngle/2.0)*(depth[:]))-shift*xRes
  
  return disp
  
  
  
def disparityToDepth(lf,disp,baseLine=None,camDistance=None,camAngle=None,xRes=None):
  """
  @author: Sven Wanner
  @brief: generates a 2d disparity image from a 2d depth image
  @param disp: <2d ndarray> disparity image
  @param baseLine: <float> camera base line in blender units
  @param camDistance: <float> distance of blender cam to scene origin
  @param camAngle: <float> blender cam opening angle in degrees
  @return: 2d ndarray
  """
  
  if baseLine is None:
    baseLine = lf.dH
  if camDistance is None:
    camDistance = lf.camDistance
  if camAngle is None:
    camAngle = lf.focalLength
  if xRes is None:
    xRes = lf.xRes
  
  shift = 1.0/(2.0*camDistance*np.tan(camAngle/2.0))*baseLine
  #calc depth
  depth = baseLine*xRes/(2.0*np.tan(camAngle/2.0)*(disp[:]+shift*xRes))
  
  return depth


def mergeDepthChannels(coh_v,coh_h,slope_v,slope_h,):
  return cm._coherenceMerge(coh_h,coh_v,slope_h,slope_v)
  