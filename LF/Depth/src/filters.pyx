##############################################################################################################################
######################                          C O N V O L U T I O N                         ################################
##############################################################################################################################  
  
  

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void adaptiveGaussianSmoothing(np.ndarray[DTYPE_t, ndim=3] image, np.ndarray[DTYPE_t, ndim=2] kernel_grid, np.ndarray[DTYPE_t, ndim=3] result):
  
  cdef int sy = image.shape[0]
  cdef int sx = image.shape[1]
  cdef int channels = image.shape[2]
  cdef index_t y,x,i
  
  cdef np.ndarray[DTYPE_t, ndim=2] tmp_c = np.zeros([sy, sx], dtype=np.float32)
  cdef np.ndarray[DTYPE_t, ndim=2] tmp = np.zeros([sy, sx], dtype=np.float32)

  for i in range(channels):
    for y in range(sy):
      for x in range(sx):
        tmp_c[<unsigned int>y,<unsigned int>x] = image[<unsigned int>y,<unsigned int>x,<unsigned int>i]
        
    convolve_x(tmp_c,kernel_grid,tmp)
    convolve_y(tmp,kernel_grid,tmp_c)
    
    for y in range(sy):
      for x in range(sx):
        result[<unsigned int>y,<unsigned int>x,<unsigned int>i] = tmp_c[<unsigned int>y,<unsigned int>x]
        
        

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void adaptiveGaussianSmoothingScalar(np.ndarray[DTYPE_t, ndim=2] image, np.ndarray[DTYPE_t, ndim=2] kernel_grid, np.ndarray[DTYPE_t, ndim=2] result):
  
  cdef int sy = image.shape[0]
  cdef int sx = image.shape[1]
  cdef index_t y,x
  
  cdef np.ndarray[DTYPE_t, ndim=2] tmp = np.zeros([sy, sx], dtype=np.float32)

  convolve_x(image,kernel_grid,tmp)
  convolve_y(tmp,kernel_grid,result)
  

  


@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void convolve_x(np.ndarray[DTYPE_t, ndim=2] image, np.ndarray[DTYPE_t, ndim=2] kernel_grid, np.ndarray[DTYPE_t, ndim=2] result):
  
  cdef index_t y,x,i
  cdef int kernel_size = kernel_grid.shape[0]
  cdef int sy = image.shape[0]
  cdef int sx = image.shape[1]
  cdef int width = <unsigned int>((kernel_size-1)/2)
  cdef float tmp = 0.0 
  cdef int waste = 0 
  
  for y in xrange(sy):
    for x in xrange(sx):
      tmp = 0.0
      if x < width:
        waste = <unsigned int>(width-x)
        for i in range(kernel_size-waste):
          tmp = tmp+image[<unsigned int>y,<unsigned int>i]*kernel_grid[<unsigned int>x,<unsigned int>i]
      if x > sx-1-width:
        waste = <unsigned int>(width-(sx-1-x))
        for i in range(kernel_size-waste):
          tmp = tmp+image[<unsigned int>y,<unsigned int>(x-width+i)]*kernel_grid[<unsigned int>((kernel_size-1)/2+waste),<unsigned int>i]
      if x>=width and x <= sx-1-width: 
        for i in range(kernel_size):
          tmp = tmp+image[<unsigned int>y,<unsigned int>(x-width+i)]*kernel_grid[<unsigned int>((kernel_size-1)/2),<unsigned int>i]
      result[<unsigned int>y,<unsigned int>x] = tmp
  


@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void convolve_y(np.ndarray[DTYPE_t, ndim=2] image, np.ndarray[DTYPE_t, ndim=2] kernel_grid, np.ndarray[DTYPE_t, ndim=2] result):
  
  cdef index_t y,x,i
  cdef int kernel_size = kernel_grid.shape[0]
  cdef int sy = image.shape[0]
  cdef int sx = image.shape[1]
  cdef int width = <unsigned int>((kernel_size-1)/2)
  cdef float tmp = 0.0 
  cdef int waste = 0 
  
  for x in xrange(sx):
    for y in xrange(sy):
      tmp = 0.0
      if y < width:
        waste = <unsigned int>(width-y)
        for i in range(kernel_size-waste):
          tmp = tmp+image[<unsigned int>i,<unsigned int>x]*kernel_grid[<unsigned int>y,<unsigned int>i]
      if y > sy-1-width:
        waste = <unsigned int>(width-(sy-1-y))
        for i in range(kernel_size-waste):
          tmp = tmp+image[<unsigned int>(y-width+i),<unsigned int>x]*kernel_grid[<unsigned int>((kernel_size-1)/2+waste),<unsigned int>i]
      if y>=width and y <= sy-1-width: 
        for i in range(kernel_size):
          tmp = tmp+image[<unsigned int>(y-width+i),<unsigned int>x]*kernel_grid[<unsigned int>((kernel_size-1)/2),<unsigned int>i]
      result[<unsigned int>y,<unsigned int>x] = tmp
  
  
  
  
##############################################################################################################################
######################                       G R A D I E N T                         #########################################
##############################################################################################################################



@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void div_x(np.ndarray[DTYPE_t, ndim=2] image, np.ndarray[DTYPE_t, ndim=2] result):
  '''
  @author: Sven Wanner
  @brief: gradient in x direction using neuman border conditions
  @param image:<2d ndarray> input image
  @param result:<2d ndarray> output image
  '''
  cdef index_t y,x
  cdef int sy = image.shape[0]
  cdef int sx = image.shape[1]
   
  for y in xrange(sy):
    for x in xrange(sx):
      if x==0:
        result[<unsigned int>y,<unsigned int>x] = image[<unsigned int>y,<unsigned int>(x+1)]-image[<unsigned int>y,<unsigned int>x]
      if x==sx-1:
        result[<unsigned int>y,<unsigned int>x] = image[<unsigned int>y,<unsigned int>x]-image[<unsigned int>y,<unsigned int>(x-1)]
      if x>0 and x<sx-1:
        result[<unsigned int>y,<unsigned int>x] = 0.5*(image[<unsigned int>y,<unsigned int>(x+1)]-image[<unsigned int>y,<unsigned int>(x-1)])



@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void div_y(np.ndarray[DTYPE_t, ndim=2] image, np.ndarray[DTYPE_t, ndim=2] result):
  '''
  @author: Sven Wanner
  @brief: gradient in y direction using neuman border conditions
  @param image:<2d ndarray> input image
  @param result:<2d ndarray> output image
  '''
  cdef index_t y,x
  cdef int sy = image.shape[0]
  cdef int sx = image.shape[1]
   
  for x in xrange(sx):
    for y in xrange(sy):
      if y==0:
        result[<unsigned int>y,<unsigned int>x] =image[<unsigned int>(y+1),<unsigned int>x]-image[<unsigned int>y,<unsigned int>x]
      if y==sy-1:
        result[<unsigned int>y,<unsigned int>x] =image[<unsigned int>y,<unsigned int>x]-image[<unsigned int>(y-1),<unsigned int>x]
      if y>0 and y<sy-1:
        result[<unsigned int>y,<unsigned int>x] = 0.5*(image[<unsigned int>(y+1),<unsigned int>x]-image[<unsigned int>(y-1),<unsigned int>x])
        
        

##############################################################################################################################
#########################                          U T I L S                         #########################################
##############################################################################################################################                 



@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void genKernelGrid(np.ndarray[DTYPE_t, ndim=2] kernels, int kernel_size, float sigma):
  '''
  @author: Sven Wanner
  @brief: generates an gaussian distribution kernel grid for boundary adaptive convolution
  @param image:<3d ndarray> input rgb image
  @param kernels:<2d ndarray> kernel grid container
  @param kernel_size:<float> kernel size
  @param sigma:<float> sigma of gaussian distribution
  '''  
  
  cdef int width = (kernel_size-1)/2
  cdef int indices_k = width+1
  cdef int index_v = 0
  cdef int index_v_cp = 0
  cdef int flip = 0
  
  cdef index_t i,j
  
  
  #container for gaussian distribution values and normalization sum
  cdef np.ndarray[DTYPE_t, ndim=1] values = np.zeros(width+1, dtype=np.float32)
  cdef np.ndarray[DTYPE_t, ndim=1] sums = np.zeros(kernel_size, dtype=np.float32)
  
  #calc gaussian distribution
  for i in xrange(width+1):
    values[<unsigned int>i] = 1./sqrt(2.0*pi*sigma*sigma)*np.exp(-<float>i*<float>i/(2.0*sigma*sigma))

  #loop over kernel grid and fill it for all necessary subkernels
  for i in xrange(kernel_size):
    index_v = index_v_cp
    flip = 0
    #loop over values of a single kernel and fill it up to its maximum length
    for j in xrange(indices_k):
      kernels[<unsigned int>i,<unsigned int>j] = values[<unsigned int>index_v]
      sums[<unsigned int>i] = sums[<unsigned int>i]+values[<unsigned int>index_v]
      if index_v > 0 and flip == 0:
        index_v = index_v - 1
      else:
        flip = 1
        index_v = index_v + 1
    if i < width:
      index_v_cp = index_v_cp + 1
        
    if i<width: indices_k = indices_k + 1
    else: indices_k = indices_k - 1

  #norm all subkernels
  for i in xrange(kernel_size):
    for j in xrange(kernel_size):
      kernels[<unsigned int>i,<unsigned int>j] = kernels[<unsigned int>i,<unsigned int>j]/sums[<unsigned int>i]
          
    


@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void setGaussKernel(np.ndarray[DTYPE_t, ndim=1] kernel,float sigma):
  cdef int size = kernel.shape[0]
  cdef float tmp = 0.0
  cdef index_t i
  
  for i in range(<unsigned int>((size-1)/2+1)):
    tmp = 1./sqrt(2.0*pi*sigma*sigma)*exp(-<float>i*<float>i/(2.0*sigma*sigma))
    kernel[<unsigned int>((size-1)/2+i)] = tmp
    kernel[<unsigned int>((size-1)/2-i)] = tmp
  normKernel(kernel)



@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void normKernel(np.ndarray[DTYPE_t, ndim=1] kernel):
  cdef float sum = 0.0
  cdef index_t i
  
  for i in range(kernel.shape[0]):
    sum = sum + kernel[<unsigned int>i]
  for i in range(kernel.shape[0]):
    kernel[<unsigned int>i] = kernel[<unsigned int>i]*1.0/sum