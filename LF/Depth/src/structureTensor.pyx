from __future__ import division
import numpy as np
cimport numpy as np
cimport cython
 

cdef extern from "math.h":
  float sqrt(float)
  float exp(float)
  float ceil(float)
  float atan2(float,float)
  float tan(float)
  
  
DTYPE = np.float32
ctypedef np.float32_t DTYPE_t
ctypedef Py_ssize_t index_t

cdef DTYPE_t pi = 3.1415926535

np.import_array()

include "filters.pyx"

"""
def testFunctions(img):
  
  res = np.zeros_like(img)
  img_bw = 0.3*img[:,:,0]+0.11*img[:,:,1]+0.59*img[:,:,2]
  res_bw = np.zeros_like(img_bw)
  res_bw2 = np.zeros_like(img_bw)
  
  output = {}
  
  #test kernels
  sigma = 3.0
  kernel_size = int(ceil( sigma * 3.0 )*2.0 +1.0)
  if (kernel_size-1)/2+1 > img.shape[0]:
    print "kernel size",kernel_size,"to big!"
    kernel_size = img.shape[0]
    print "new kernel size",kernel_size 
    
  kernel_grid = np.zeros((kernel_size,kernel_size),dtype=np.float32)
  genKernelGrid(kernel_grid,kernel_size,sigma)
  
  s = ""
  for i in range(kernel_size):
    s += str(i)+"->"
    for j in range(kernel_size):
      s+= str(kernel_grid[i,j])+","
    s+="  sum:"+str(np.sum(kernel_grid[i,:]))+"/n"
  output["kernel_grid"] = s
    
  
  adaptiveGaussianSmoothing(img,kernel_grid,res)
  output["gauss_rgb"] = np.copy(res)
  
  
  adaptiveGaussianSmoothingScalar(img_bw,kernel_grid,res_bw)
  output["gauss_bw"] = np.copy(res_bw)
  
  div_x(img_bw,res_bw)
  output["div_x"] = np.copy(res_bw)
  
  div_y(img_bw,res_bw)
  output["div_y"] = np.copy(res_bw)
  
  
  #test kernels
  inner = 0.8
  kernel_size = int(ceil( inner * 3.0 )*2.0 +1.0)
  if (kernel_size-1)/2+1 > img.shape[0]:
    print "inner kernel size",kernel_size,"to big!"
    kernel_size = img.shape[0]
    print "new inner kernel size",kernel_size
  inner_kernel_grid = np.zeros((kernel_size,kernel_size),dtype=np.float32)
  genKernelGrid(inner_kernel_grid,kernel_size,inner)
  outer = 1.3
  kernel_size = int(ceil( outer * 3.0 )*2.0 +1.0)
  if (kernel_size-1)/2+1 > img.shape[0]:
    print "outer kernel size",kernel_size,"to big!"
    kernel_size = img.shape[0]
    print "outer new kernel size",kernel_size
  outer_kernel_grid = np.zeros((kernel_size,kernel_size),dtype=np.float32)
  genKernelGrid(outer_kernel_grid,kernel_size,outer)
  res = np.zeros_like(img)
  adaptiveBoundaryStructureTensor(img,inner_kernel_grid,outer_kernel_grid,res)
  
  output["st"] = np.copy(res)
  
  structureTensorSlopeCoherence(res,res_bw,res_bw2,-1.5,1.5,0)
  output["coh"] = np.copy(res_bw)
  output["slope"] = np.copy(res_bw2)
  
  return output
""" 
  
  
  
  
  
  
  
@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
def calcSubLFDisparityAndCoherence_RGB(np.ndarray[DTYPE_t, ndim=4] subLF, int direction, float inner, float outer, float minLabel, float maxLabel, np.ndarray[DTYPE_t, ndim=3] coherence, np.ndarray[DTYPE_t, ndim=3] slope):
  '''
  @author: Sven Wanner
  @brief: calculates the slope, coherence of a 3D RGB light field 
  @param subLF:<4d ndarray> input sub light field
  @param direction:<int> 0 correlates with vertical, 1 with horizontal camera movement
  @param inner:<float> inner scale for structure tensor
  @param outer:<float> outer scale for structure tensor
  @param minLabel:<float> min clamp boundary
  @param maxLabel:<float> max clamp boundary
  @param coherence:<3d ndarray> output coherence volume
  @param slope:<3d ndarray> output slope volume  
  '''
  cdef index_t a,l,s,c
  cdef int angles = subLF.shape[0]
  cdef int loop,slice
  cdef int inner_kernel_size
  cdef int outer_kernel_size
  
  if direction == 0:
    loop = subLF.shape[2]
    slice = subLF.shape[1]
  else:
    loop = subLF.shape[1]
    slice = subLF.shape[2]
      
  cdef np.ndarray[DTYPE_t, ndim=3] tmp_epi = np.zeros([angles, slice,3], dtype=DTYPE)
  cdef np.ndarray[DTYPE_t, ndim=3] tmp_st = np.zeros([angles, slice,3], dtype=DTYPE)
  cdef np.ndarray[DTYPE_t, ndim=2] tmp_coh = np.zeros([angles, slice], dtype=DTYPE)
  cdef np.ndarray[DTYPE_t, ndim=2] tmp_slope = np.zeros([angles, slice], dtype=DTYPE)
  
  #define inner kernel_grid
  inner_kernel_size = <int>(ceil( inner * 3.0 )*2.0 +1.0)
  if inner_kernel_size > angles:
    inner_kernel_size = angles
  cdef np.ndarray[DTYPE_t, ndim=2] inner_kernel_grid = np.zeros((inner_kernel_size,inner_kernel_size),dtype=np.float32)
  genKernelGrid(inner_kernel_grid,inner_kernel_size,inner)
  
  
  #define outer kernel_grid
  outer_kernel_size = <int>(ceil( outer * 3.0 )*2.0 +1.0)
  if outer_kernel_size > angles:
    outer_kernel_size = angles
  cdef np.ndarray[DTYPE_t, ndim=2] outer_kernel_grid = np.zeros((outer_kernel_size,outer_kernel_size),dtype=np.float32)
  genKernelGrid(outer_kernel_grid,outer_kernel_size,outer)
  
  #loop over all epis in sub lf
  for l in xrange(loop):
    #get an epi from lf
    for a in xrange(angles):
      for s in xrange(slice):
        for c in range(3):
          if direction == 0:
            tmp_epi[<unsigned int>a,<unsigned int>s,<unsigned int>c] = subLF[<unsigned int>a,<unsigned int>s,<unsigned int>l,<unsigned int>c]
          else:
            tmp_epi[<unsigned int>a,<unsigned int>s,<unsigned int>c] = subLF[<unsigned int>a,<unsigned int>l,<unsigned int>s,<unsigned int>c]
            
    #calc slope and coherence
    adaptiveBoundaryStructureTensor(tmp_epi,inner_kernel_grid,outer_kernel_grid,tmp_st)
    structureTensorSlopeCoherence(tmp_st,tmp_coh,tmp_slope,minLabel,maxLabel,direction)
    
    #put results in output arrays
    for a in xrange(angles):
      for s in xrange(slice):
        if direction == 0:
          coherence[<unsigned int>a,<unsigned int>s,<unsigned int>l] = tmp_coh[<unsigned int>a,<unsigned int>s]
          slope[<unsigned int>a,<unsigned int>s,<unsigned int>l] = tmp_slope[<unsigned int>a,<unsigned int>s]
        else:
          coherence[<unsigned int>a,<unsigned int>l,<unsigned int>s] = tmp_coh[<unsigned int>a,<unsigned int>s]
          slope[<unsigned int>a,<unsigned int>l,<unsigned int>s] = tmp_slope[<unsigned int>a,<unsigned int>s]
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
##############################################################################################################################
###############                          S T R U C T U R E   T E N S O R                         #############################
##############################################################################################################################

  
@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void adaptiveBoundaryStructureTensor(np.ndarray[DTYPE_t, ndim=3] image, np.ndarray[DTYPE_t, ndim=2] innerScale_kernelGrid, np.ndarray[DTYPE_t, ndim=2] outerScale_kernelGrid, np.ndarray[DTYPE_t, ndim=3] result):
  '''
  @author: Sven Wanner
  @brief: calculates the structure tensor on RGB images based on adaptive boundary kernels 
  @param image:<3d ndarray> input RGB image
  @param innerScale_kernelGrid:<float> inner smoothing parameter kernel grid
  @param outerScale_kernelGrid:<float> outer smoothing parameter kernel grid
  @param result:<3d ndarray> output structure tensor image
  '''
  cdef index_t y,x,c
  cdef int sy = image.shape[0]
  cdef int sx = image.shape[1]
  cdef int channels = image.shape[2]
  
  cdef np.ndarray[DTYPE_t, ndim=3] tmp = np.zeros([sy, sx, channels], dtype=np.float32)
  cdef np.ndarray[DTYPE_t, ndim=2] tmp_div = np.zeros([sy, sx], dtype=np.float32)
  cdef np.ndarray[DTYPE_t, ndim=2] tmp_divx = np.zeros([sy, sx], dtype=np.float32)
  cdef np.ndarray[DTYPE_t, ndim=2] tmp_divy = np.zeros([sy, sx], dtype=np.float32)
  
  cdef np.ndarray[DTYPE_t, ndim=4] tmp_st = np.zeros([channels, sy, sx, 3], dtype=np.float32)



    
  #smooth input image using innerScale
  adaptiveGaussianSmoothing(image,innerScale_kernelGrid,tmp)
  
  for c in range(channels):
    #fill smoothed channel
    for y in range(sy):
      for x in range(sx):
        tmp_div[<unsigned int>y,<unsigned int>x] = tmp[<unsigned int>y,<unsigned int>x,<unsigned int>c]
        
    #calc gradients
    div_x(tmp_div,tmp_divx)
    div_y(tmp_div,tmp_divy)

    #calc structure tensor components 
    for y in range(sy):
      for x in range(sx):
        tmp_st[<unsigned int>c,<unsigned int>y,<unsigned int>x,2] = tmp_divx[<unsigned int>y,<unsigned int>x]*tmp_divx[<unsigned int>y,<unsigned int>x]
        tmp_st[<unsigned int>c,<unsigned int>y,<unsigned int>x,1] = tmp_divx[<unsigned int>y,<unsigned int>x]*tmp_divy[<unsigned int>y,<unsigned int>x]
        tmp_st[<unsigned int>c,<unsigned int>y,<unsigned int>x,0] = tmp_divy[<unsigned int>y,<unsigned int>x]*tmp_divy[<unsigned int>y,<unsigned int>x]
  
  #put mean of all structure tensor channels into result
  for c in range(channels):
    for y in range(sy):
      for x in range(sx):
        tmp_divx[<unsigned int>y,<unsigned int>x] = (tmp_st[<unsigned int>0,<unsigned int>y,<unsigned int>x,<unsigned int>c]+tmp_st[<unsigned int>1,<unsigned int>y,<unsigned int>x,<unsigned int>c]+tmp_st[<unsigned int>2,<unsigned int>y,<unsigned int>x,<unsigned int>c])/3.0
    
    #smooth tensor component using outerScale
    adaptiveGaussianSmoothingScalar(tmp_divx,outerScale_kernelGrid,tmp_div)
    
    for y in range(sy):
      for x in range(sx): 
        result[<unsigned int>y,<unsigned int>x,<unsigned int>c] = tmp_div[<unsigned int>y,<unsigned int>x]
        
        
        
  
@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef inline void structureTensorSlopeCoherence(np.ndarray[DTYPE_t, ndim=3] tensor, np.ndarray[DTYPE_t, ndim=2] coherence, np.ndarray[DTYPE_t, ndim=2] slope, float minLabel, float maxLabel, int direction):
  '''
  @author: Sven Wanner
  @brief: calculates the slope, coherence and the invalid pixels from a structure tensor 
  @param tensor:<3d ndarray> input structure tensor
  @param coherence:<2d ndarray> output coherence image
  @param slope:<2d ndarray> output slope image
  @param minLabel:<float> min clamp boundary
  @param maxLabel:<float> max clamp boundary
  '''
  cdef index_t y,x
  cdef int sign = -1
  cdef int sy = tensor.shape[0]
  cdef int sx = tensor.shape[1]
  
  
  if direction:
    sign = 1
    
  
  for y in range(sy):
    for x in range(sx):
      coherence[<unsigned int>y,<unsigned int>x] = sqrt((tensor[<unsigned int>y,<unsigned int>x,2]-tensor[<unsigned int>y,<unsigned int>x,0])*(tensor[<unsigned int>y,<unsigned int>x,2]-tensor[<unsigned int>y,<unsigned int>x,0])+2*tensor[<unsigned int>y,<unsigned int>x,1]*tensor[<unsigned int>y,<unsigned int>x,1])/(tensor[<unsigned int>y,<unsigned int>x,2]+tensor[<unsigned int>y,<unsigned int>x,0]+1e-16)
      slope[<unsigned int>y,<unsigned int>x] = 0.5 * atan2( 2.0 *tensor[<unsigned int>y,<unsigned int>x,1],tensor[<unsigned int>y,<unsigned int>x,2]-tensor[<unsigned int>y,<unsigned int>x,0])
      slope[<unsigned int>y,<unsigned int>x] = tan(-slope[<unsigned int>y,<unsigned int>x])
      if sign*slope[<unsigned int>y,<unsigned int>x] > maxLabel:
        slope[<unsigned int>y,<unsigned int>x] = sign*maxLabel
        coherence[<unsigned int>y,<unsigned int>x] = 0
      if sign*slope[<unsigned int>y,<unsigned int>x] < minLabel:
        slope[<unsigned int>y,<unsigned int>x] = sign*minLabel
        coherence[<unsigned int>y,<unsigned int>x] = 0
  
  






                          
                          
     
     
     

  
  
  
  
  





























