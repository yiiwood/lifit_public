import traceback
from sys import exit
from types import StringType, IntType, ListType, TupleType, FloatType



try: from numpy import zeros, copy, uint8, float32, transpose, amin, amax, mean, sqrt, round, ceil
except: 
    print "seems you haven't installed numpy: on Ubuntu you can do this via($ sudo apt-get install python-numpy)"
    exit()
try: 
    from scipy.ndimage import shift
    from scipy.ndimage.interpolation import zoom
except: 
    print "seems you haven't installed scipy: on Ubuntu you can do this via($ sudo apt-get install python-scipy)"
    exit()
try: 
    import pylab as plt
    import matplotlib.cm as cm
except: 
    print "seems you haven't installed matplotlib: on Ubuntu you can do this via($ sudo apt-get install python-matplotlib)"
    exit()
try: from h5py import File
except: 
    print "seems you haven't installed h5py: on Ubuntu you can do this via($ sudo apt-get install python-h5py)"
    exit()


from LF.settings import *
    
class LFError(Exception):
  def __init__(self, value):
    self.value = value
  def __str__(self):
    return repr(self.value)

class LazyLF(object):
  
  def __init__(self, h5d):
    self.h5d = h5d
    self.shape = h5d.shape
    self.dtype = h5d.dtype
    
  def __getitem__(self, key):
    if (key == slice(None, None, None)):
      # traceback.print_stack()
      if DEBUG == 2: print "WARNING: Accessing complete lightfield dataset! "
    return self.h5d[key]
    
  def __setitem__(self, key, value):
    self.h5d[key] = value
    

class LightField(object):
  """
  @author: Sven Wanner
  @summary: Base LightField class
  """
  def __init__(self, channels=1, h5f=None):  
    self._h5f = h5f
    self.__type__ = "LightField"
    self.channels = channels  # number of color channels
    self.xRes = 0  # data extension in x = shape(LF)[3]
    self.yRes = 0  # data etension in y = shape(LF)[2]
    self.hRes = 1  # angular resolution in horizontal direction
    self.vRes = 1  # angular resolution in vertical direction
    self.dV = 0  # vertical baseline
    self.dH = 0  # horizontal baseline
    self.focalLength = None  # camera focal length
    self.camDistance = None  # for simulated light field distance to scene center
    self.vSampling = zeros((1, 1), dtype=float32)  # relative positions in vertical angular direction
    self.hSampling = zeros((1, 1), dtype=float32)  # relative positions in horizontal angular direction
    self.lf = zeros((1, 1, 1, 1, self.channels), dtype=uint8)  # 4D raw  data in (v,u,y,x,channels)
    self.depth = None  # dataset for depth results
    self.inner = None  # used innerScale for depth estimation
    self.outer = None  # used outerScale for depth estimation
    self.cvOnly = None  # True if only center depth view is stored
    self.tv = None  # parameter of tv regularization
    self.gt = None  # dataset for ground truth data
    self.location = None # filepath of light field
  
  def __del__(self):
    self.close()
    
  def __str__(self):
    shape=str(self.vRes)+","+str(self.hRes)+","+str(self.yRes)+","+str(self.xRes)+","+str(self.channels)
    depth="yes"
    if self.depth is None:
      depth = "no"
    gt="yes"
    if self.gt is None:
      gt = "no"
    if self.camDistance is not None:
      cd = str(self.camDistance)
    else:
      cd = "no"
    if self.focalLength is not None:
      fl = str(self.focalLength)
    else:
      fl="no"
    return "LightField: \nvRes:"+str(self.vRes)+"\nhRes:"+str(self.hRes)+"\nyRes:"+str(self.yRes)+"\nxRes:"+str(self.xRes)+"\nchannels:"+str(self.channels)+"\ndepth:"+depth+"\ngt:"+gt+"\ncamDistance:"+cd+"\nfocalLength:"+fl
    
  def close(self):
    if self._h5f != None:
      self._h5f.close()
      
  def load(self,fname):
    """
    @author: Sven Wanner
    @summary: loads LightField data from file
    @param fname: <string> filename
    """
    if type(fname) == StringType:
      if DEBUG>0: print "\nLoading Light Field:", fname
    else:
      print "Argument Type Error: Cannot handle", type(fname), "in loadLF"
      exit()
  
    try:
      f = File(fname, 'r') 
    except:  
      raise Exception("HDF5 read/write error")
    try:
      if DEBUG>0: print "try to load light field data ... ",
      self.lf = LazyLF(f["LF"])
      if DEBUG>0: print "ok"
    except:
      raise Exception("Fatal Error: No LF available!")
    try:
      if DEBUG>0: print "try to load depth data ... ",
      self.depth = LazyLF(f["Depth"])
      if DEBUG>0: print "ok"
    except:  
      if DEBUG>0: print "no depth data available"
    try:
      if DEBUG>0: print "try to load ground truth data ... ",
      self.gt = LazyLF(f["GT"])
      if DEBUG>0: print "ok"
    except:  
      if DEBUG>0: print "no ground truth data available"
  
  
    self.xRes = copy(f.attrs["xRes"])
    self.yRes = copy(f.attrs["yRes"])
    self.vRes = copy(f.attrs["vRes"])
    self.hRes = copy(f.attrs["hRes"])
    self.dV = copy(f.attrs["dV"])
    self.dH = copy(f.attrs["dH"])
    self.vSampling = copy(f.attrs["vSampling"])
    self.hSampling = copy(f.attrs["hSampling"])
    self.channels = copy(f.attrs["channels"])
    
    try:
      self.focalLength = copy(f.attrs["focalLength"])
    except:
      self.focalLength = None
    try:
      self.location = fname
    except:
      self.location = None
    try:
      self.camDistance = copy(f.attrs["camDistance"])
    except:
      self.camDistance = None
    try:
      self.inner = copy(f.attrs["inner"])
    except:
      self.inner = None
    try:
      self.outer = copy(f.attrs["outer"])
    except:
      self.outer = None
    try:
      self.cvOnly = copy(f.attrs["cvOnly"])
    except:
      self.cvOnly = None
    try:
      self.tv = copy(f.attrs["tv"])
    except:
      self.tv = None
      
    return self
    if DEBUG>0: print "Done\n"
      
   
  def save(self, outpath=None):
    """
    @author: Sven Wanner
    @summary: saves LightField instance 
    @param outpath: <string> filename
    """
    
    if outpath is None and self.location is not None:
      outpath = self.location
    elif outpath is None and self.location is None:
      print "Unable to save Lightfield without destination"
      fname = raw_input("Where to store the result? (a) abort")
      if fname == "a":
        import sys
        sys.exit()
      else:
        if fname.find(".h5") == -1:
          fname += ".h5"
        outpath = fname
      return
  
    try:
      if DEBUG>0: print "\nSaving Light Field:", outpath
      f = File(outpath, 'w')
    except KeyError:
      raise Exception("HDF5 read/write error")
    
    if DEBUG>0: print "write light field...",
    f.create_dataset("LF", data=self.lf[:], dtype=uint8, compression='gzip', chunks=(1, 1, self.lf.shape[2], self.lf.shape[3], self.lf.shape[4]))
    if DEBUG>0: print "ok"
    if self.depth is not None and len(self.depth.shape) == 4:
      if DEBUG>0: print "write entire depth estimation...",
      f.create_dataset("Depth", data=self.depth[:], dtype=float32, compression='gzip', chunks=(1, 1, self.depth.shape[2], self.depth.shape[3]))
      if DEBUG>0: print "ok"
    if self.depth is not None and len(self.depth.shape) == 2:
      if DEBUG>0: print "write cv depth estimation...",
      f.create_dataset("Depth", data=self.depth[:], dtype=float32, compression='gzip', chunks=(self.depth.shape[0], self.depth.shape[1]))
      if DEBUG>0: print "ok"
    if self.gt is not None and len(self.gt.shape) == 4:
      if DEBUG>0: print "write entire ground truth...",
      f.create_dataset("GT", data=self.gt[:], dtype=float32, compression='gzip', chunks=(1, 1, self.gt.shape[2], self.gt.shape[3]))
      if DEBUG>0: print "ok"
    if self.gt is not None and len(self.gt.shape) == 2:
      if DEBUG>0: print "write cv ground truth...",
      f.create_dataset("GT", data=self.gt[:], dtype=float32, compression='gzip', chunks=(self.gt.shape[0], self.gt.shape[1]))
      if DEBUG>0: print "ok"
      
      
    f.attrs["xRes"] = self.xRes
    f.attrs["yRes"] = self.yRes
    f.attrs["vRes"] = self.vRes
    f.attrs["hRes"] = self.hRes
    f.attrs["dV"] = self.dV
    f.attrs["dH"] = self.dH
    f.attrs["channels"] = self.channels
    if self.focalLength is not None:
      f.attrs["focalLength"] = self.focalLength
    if self.camDistance   is not None: 
      f.attrs["camDistance"] = self.camDistance  
    if self.inner is not None:
      f.attrs["inner"] = self.inner
    if self.outer is not None:
      f.attrs["outer"] = self.outer
    if self.cvOnly is not None:
      f.attrs["cvOnly"] = self.cvOnly
    if self.tv is not None:
      f.attrs["tv"] = self.tv
  
    f.attrs["vSampling"] = self.vSampling
    f.attrs["hSampling"] = self.hSampling
    
    
    f.close()
    if DEBUG>0: print "Done\n"
    
  
  def copy(self):
    """
    @author: Sven Wanner
    @summary: copy LightField instance
    @return: <LF> LightField instance
    """

    if DEBUG>0: print "\nCopy Light Field"
    lf = LightField()
    lf.xRes = self.xRes
    lf.yRes = self.yRes
    lf.vRes = self.vRes
    lf.hRes = self.hRes
    lf.dV = self.dV
    lf.dH = self.dH
    lf.vSampling = self.vSampling 
    lf.hSampling = self.hSampling 
    lf.lf = copy(self.lf[:])
    
    if self.depth is not None:
      lf.depth = copy(self.depth[:])
    if self.gt is not None:
      lf.gt = copy(self.gt[:])
      
    if self.camDistance is not None:
      print "copy camDistance",self.camDistance
      lf.camDistance = self.camDistance    
    if self.inner is not None:
      print "copy inner",self.inner
      lf.inner = self.inner
    if self.outer is not None:
      print "copy outer",self.outer
      lf.outer = self.outer
    if self.cvOnly is not None:
      print "copy cvOnly",self.cvOnly
      lf.cvOnly = self.cvOnly
    if self.tv is not None:
      print "copy tv",self.tv
      lf.tv = self.tv
    if self.focalLength is not None:
      print "copy focalLength",self.focalLength
      lf.focalLength = self.focalLength
    if self.location is not None:
      lf.location = self.location
    lf.channels = self.channels
    if DEBUG>0: print "Done\n"
    
    return lf
  
  
  def show(self, view=None, epi_v=None, epi_h=None, stretch=5, saveTo=None, show=True):
    """
    @author: Sven Wanner
    @summary: simple light field viewer, default show is center view and center epis
    @param lf: LightField instance
    @param view: <int list> view[0] column view[1] row, default:None
    @param view: <int> vertical epi cut column, default:None
    @param view: <int> horizontal epi cut row, default:None
    @param stretch: <float> epi stretch value, default:1
    @param saveTo: <string> save location, if None only show, default:None
    @param show: <bool> show switch, default: True
    """
  
    if view is None:
      view = [self.vRes / 2, self.hRes / 2]
    if epi_v is None:
      epi_v = self.xRes / 2
    if epi_h is None:
      epi_h = self.yRes / 2
      
    if stretch <= 0:
      stretch = 1
      
    if self.channels == 3:
      view_im = self.lf[view[0], view[1], :, :, :]
    else:
      view_im = self.lf[view[0], view[1], :, :, 0]
    
    
    view_im = zeros((self.yRes, self.xRes, self.channels), dtype=uint8)
    for i in range(self.channels):
      view_im[:, :, i] = self.lf[view[0], view[1], :, :, i]

      # draw epi markers
      if i == 0:
        try:  
          view_im[:, epi_v - 1, i] = 200
          view_im[:, epi_v + 1, i] = 200
          view_im[epi_h - 1, :, i] = 25
          view_im[epi_h + 1, :, i] = 25
        except:
          pass
        view_im[:, epi_v, i] = 255
        view_im[epi_h, :, i] = 0
      elif i == 1:
        try:
          view_im[epi_h - 1, :, i] = 200
          view_im[epi_h + 1, :, i] = 200
          view_im[:, epi_v - 1, i] = 25
          view_im[:, epi_v + 1, i] = 25
        except:
          pass
        view_im[epi_h, :, i] = 255
        view_im[:, epi_v, i] = 0
      else:
        try:
          view_im[:, epi_v + 1, i] = 25
          view_im[:, epi_v - 1, i] = 25
          view_im[:, epi_v + 1, i] = 25
          view_im[:, epi_v - 1, i] = 25
        except:
          pass
        view_im[:, epi_v, i] = 0
        view_im[:, epi_v, i] = 0
    
    # extract horizonal epi
    epiH_im = zeros((stretch * self.hRes, self.xRes, self.channels), dtype=uint8)
    for i in range(self.channels):
      tmp = copy(self.lf[view[0], :, epi_h, :, i])
      epiH_im[:, :, i] = zoom(tmp, zoom=(stretch, 1))
      
    # extract vertical epi 
    epiV_im = zeros((self.yRes, stretch * self.vRes, self.channels), dtype=uint8)
    for i in range(self.channels):
      tmp = copy(transpose(self.lf[:, view[1], :, epi_v, i]))
      epiV_im[:, :, i] = zoom(tmp, zoom=(1, stretch))
      
    # fill result image
    img_sy = int(ceil(1.02 * view_im.shape[0] + epiH_im.shape[0]))
    img_sx = int(ceil(1.02 * view_im.shape[1] + epiV_im.shape[1]))
    
    print "img_sy",img_sy
    print "img_sx",img_sx
    
    img = zeros((img_sy, img_sx, self.channels), dtype=uint8)
    
    print "img.shape",img.shape
    
    print "\nepiV_im.shape",epiV_im.shape
    print "view_im.shape",view_im.shape
    
    print "img[0:view_im.shape[0], 0:view_im.shape[1], :].shape",img[0:view_im.shape[0], 0:view_im.shape[1], :].shape
    
    img[0:view_im.shape[0], 0:view_im.shape[1], :] = view_im[:]
    img[img.shape[0] - epiH_im.shape[0] - 1:-1, 0:epiH_im.shape[1], :] = epiH_im[:]
    img[0:epiV_im.shape[0], img.shape[1] - epiV_im.shape[1] - 1:-1, :] = epiV_im[:]
    
    # add a border
    gap = int(round(img_sx - view_im.shape[0] - epiH_im.shape[0]))
    result = zeros((img.shape[0] + 2 * gap, img.shape[1] + 2 * gap, self.channels), dtype=uint8)
    
    result[gap:img.shape[0] + gap, gap:img.shape[1] + gap] = img[:]
    
    
    
    # plot
    fig = plt.figure()
    
    ax = fig.add_subplot(111)
    ax.set_title("view: [v,h] = [" + str(view[0]) + "," + str(view[1]) + "] epi_v at x =" + str(epi_v) + " epi_h at y =" + str(epi_h))
  
    if self.channels == 1:
      result = result[:, :, 0]
    ax.imshow(result, interpolation='nearest')
    
    if saveTo is not None and type(saveTo) is StringType:
      if saveTo.find(".png") == -1:
        saveTo += ".png"
      if DEBUG>0: print "Save figure to:", saveTo
      plt.savefig(saveTo, transparent=False)
    
    if show: plt.show()
  


  def showDepth(self, view=None, cmap="jet", saveTo=None, show=True):
    
    if cmap == "gray":
      cmap = cm.gray
    elif cmap == "jet":
      cmap = cm.jet
    elif cmap == "hot":
      cmap = cm.hot
    elif cmap == "autumn":
      cmap = cm.autumn
 
    if self.depth is None:
      if DEBUG>0: print "No depth data available"
      return
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    
    view = [self.vRes / 2, self.hRes / 2]
    
    if self.cvOnly:
      d = self.depth
    else:
      if view is None:
        view = [self.vRes / 2, self.hRes / 2]
    d = self.depth[view[0], view[1]]
    ext = [amin(d),amax(d)]
    cax = ax.imshow(d, interpolation='nearest', cmap=cmap)
    #ax.set_xlabel("(" + str(amin(self.depth)) + "," + str(amax(self.depth)) + ") mean=" + str(mean(self.depth)))

    cbar = fig.colorbar(cax, ticks=[ext[0], 0, ext[1]], orientation='vertical')
    
    if saveTo is not None and type(saveTo) is StringType:
      if saveTo.find(".png") == -1:
        saveTo += ".png"
      if DEBUG>0: print "Save figure to:", saveTo
      plt.savefig(saveTo, transparent=False)
    
    if show: plt.show()
      
  
  def refocusLF(self, shifts=None):
    """
    @author: Sven Wanner
    @summary: refocus a light field 
    @param shifts: <float/2d list of floats> shift values, default: None
    @return: LightField instance
    """
    
    if DEBUG>0: print "\nShift LightField"
    outLF = self.copy()
    
    if shifts is None or shifts == 0:
      if DEBUG>0: print "Warning, no shift given, non refocused light field is returned"
      return outLF
    
    if type(shifts) == type(1.0) or type(shifts) == type(1):
      shifts = (shifts, shifts)
    
    if type(shifts) == type([]) or type(shifts) == type(()):
      n = self.vRes*self.hRes
      for v in range(self.vRes):
        for h in range(self.hRes):
          if DEBUG>0: print "\rshifts to do:",n,
          n-=1
          # calc normed img position vector
          imPos = [self.vSampling[v, h] / self.dV, self.hSampling[v, h] / self.dH]
          
          shiftVals = [shifts[0] * imPos[0], shifts[1] * imPos[1]]
          
          for c in range(self.channels):
            outLF.lf[v, h, :, :, c] = shift(self.lf[v, h, :, :, c], shiftVals)
      print ""
    else:
      if DEBUG>0: print "Warning, shift type error, non refocused light field is returned"
    
    return outLF

"""

def denoiseDepth(lf, weight, iter):
  from LFLib.ImageProcessing.filter import tv_regularizer
  
  for i in range(lf.vRes):
    for j in range(lf.hRes):
      for c in range(3):
        lf.depth[i, j, :, :] = tv_regularizer(lf.depth[i, j, :, :], weight, iter)
        
  return lf
  
  
def readOutLF(LFin, loc):
  from scipy.misc import imsave
  from Helpers import ensure_dir
  
  gt_dir = None
  depth_dir = None
  
  lf_dir = ensure_dir(loc + "lf/")
  if LFin.depth is not None and len(LFin.depth.shape) > 2:
    depth_dir = ensure_dir(loc + "depth/")
  if LFin.gt is not None and len(LFin.gt.shape) > 2:
    gt_dir = ensure_dir(loc + "gt/")
  
  for i in range(LFin.vRes):
    for j in range(LFin.hRes):
      imsave(lf_dir + "/img_%3.3i_%3.3i.png" % (i, j), LFin.lf[i, j, :, :, :])
      if depth_dir is not None:
        imsave(depth_dir + "/depth_%3.3i_%3.3i.png" % (i, j), LFin.depth[i, j, :, :])
      if gt_dir is not None:
        imsave(gt_dir + "/gt_%3.3i_%3.3i.png" % (i, j), LFin.gt[i, j, :, :])
        
        
        
        
def saveDepthChannels(channels, outpath=None):

  @author: Sven Wanner
  @summary: saves a depth channel data structure
  @param LFin: <LF> LightField instance
  @param outpath: <string> filename

  try:
    print "\nSaving depth channels:", outpath
    f = File(outpath, 'w')
  except KeyError:
    raise LFError("HDF5 read/write error")
  
  print "write channels"
  f.create_dataset("depth_h", data=channels[:, :, :, :, 0], dtype=float32, compression='gzip')
  f.create_dataset("coherence_h", data=channels[:, :, :, :, 1], dtype=float32, compression='gzip')
  f.create_dataset("depth_v", data=channels[:, :, :, :, 2], dtype=float32, compression='gzip')
  f.create_dataset("coherence_v", data=channels[:, :, :, :, 3], dtype=float32, compression='gzip')  
  
  f.close()
  print "Done"
  
"""
