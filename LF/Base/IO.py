import sys
from LF.Helpers import ensure_dir

try: 
    from scipy.misc import imsave
except: 
    print "seems you haven't installed scipy: on Ubuntu you can do this via($ sudo apt-get install python-scipy)"
    sys.exit()

def readOutLF(LFin,loc):
  
  gt_dir = None
  depth_dir = None
  
  lf_dir = ensure_dir(loc+"lf/")
  if LFin.depth is not None and len(LFin.depth.shape)>2:
    depth_dir = ensure_dir(loc+"depth/")
  if LFin.gt is not None and len(LFin.gt.shape)>2:
    gt_dir = ensure_dir(loc+"gt/")
  
  for i in range(LFin.vRes):
    for j in range(LFin.hRes):
      imsave(lf_dir+"/img_%3.3i_%3.3i.png"%(i,j),LFin.lf[i,j,:,:,:])
      if depth_dir is not None:
        imsave(depth_dir+"/depth_%3.3i_%3.3i.png"%(i,j),LFin.depth[i,j,:,:])
      if gt_dir is not None:
        imsave(gt_dir+"/gt_%3.3i_%3.3i.png"%(i,j),LFin.gt[i,j,:,:])
        