import os
from LF.settings import *
from LF.Base.LightField import *
from LF.Helpers import ensure_dir
from LF.ImageProcessing.UI import *



try: import numpy as np
except: 
    print "seems you haven't installed numpy: on Ubuntu you can do this via($ sudo apt-get install python-numpy)"
    exit()
try: 
    from scipy.misc import imread, imsave
except: 
    print "seems you haven't installed scipy: on Ubuntu you can do this via($ sudo apt-get install python-scipy)"
    exit()
try: 
    import h5py as h5
except: 
    print "seems you haven't installed h5py: on Ubuntu you can do this via($ sudo apt-get install python-h5py"
    exit()



class Cam:
    """!
    @author Sven Wanner
    @brief camera object - holds calibration data
    """
    def __init__(self,cmat=np.zeros( (3,3) ), dist=np.zeros( (1,5) ), rot=np.zeros( (1,3) ), trans=np.zeros( (1,3) ) ):
      self.cmat=cmat
      self.dist=dist
      self.rot=rot
      self.trans=trans
      
    def copy(self):
      return Cam(self.cmat,self.dist,self.rot,self.trans)
    
    def save(self,filename):
      try:
        if DEBUG>0: print "save cam object...",
        f = h5.File(filename,  'w')
        f.create_dataset("cmat", data=np.asarray(self.cmat ))
        f.create_dataset("dist", data=np.asarray(self.dist))
        f.create_dataset("rot", data=np.asarray(self.rot))
        f.create_dataset("trans", data=np.asarray(self.trans))
        f.close()
        if DEBUG>0: print "ok"
      except KeyError:
        raise Exception("HDF5 read/write error")
      
    def load(self,filename):
      try:
        if DEBUG>0: print "load cam object...",
        f = h5.File(filename, 'r')
        self.cmat=np.copy(f["cmat"])
        self.dist=np.copy(f["dist"])
        self.rot=np.copy(f["rot"])
        self.trans=np.copy(f["trans"])
        f.close()
        if DEBUG>0: print "ok"
        return self
      except KeyError:
        raise Exception("HDF5 read/write error")
      
      

class Gantry:
    """!
    @author Sven Wanner
    @brief gantry object - holds gantry (cam array) calibration data
    """
    def __init__(self,trans=[],rot=[],homs=[],grid=(0,0)):
      self.trans=trans
      self.rot=rot
      self.homs=homs
      self.grid=grid
      
    def copy(self):
      return Gantry(self.trans,self.rot,self.homs, self.grid)
    
    def save(self,filename):
      try:
        if DEBUG>0: print "save gantry object...",
        f = h5.File(filename, 'w')
        f.create_dataset("trans", data=np.asarray(self.trans))
        f.create_dataset("rot", data=np.asarray(self.rot))
        f.create_dataset("homs", data=np.asarray(self.homs))
        f.create_dataset("grid", data=np.asarray(self.grid))
        f.close()
        if DEBUG>0: print "ok"
      except KeyError:
        raise Exception("HDF5 read/write error")
      
    def load(self,filename):
      try:
        if DEBUG>0: print "load gantry object...",
        f = h5.File(filename, 'r')
        self.trans=np.copy(f["trans"])
        self.rot=np.copy(f["rot"])
        self.homs=np.copy(f["homs"])
        self.grid=np.copy(f["grid"])
        f.close()
        if DEBUG>0: print "ok"
        return self
      except KeyError:
        raise Exception("HDF5 read/write error")
      
      
      

def reorderImagesFromGantry(files,save,grid):
  """
  @author: Sven Wanner
  @brief: reorder images from gantry to the common HCI order
  @param files:<str> path to the gantry images
  @param save:<str> path to store the reordered images
  @param grid:<list> values of sampling grid
  """
  if not files.endswith("/"): files+="/"
  if not save.endswith("/"): save+="/"
  save = ensure_dir(save)
  flist = os.listdir(files)
  flist.sort()
  
  nlist = []
  
  n=0
  for v in range(grid[0]):
    imgs = []
    for h in range(grid[1]):
      imgs.append(flist[n])
      n+=1
    if v%2==0:
      nlist.append(imgs)
    if v%2!=0:
      imgs.reverse()
      nlist.append(imgs)
  
  n=0
  for i in range(len(nlist)):
    for j in range(len(nlist[i])):
      fname = save+"img_%3.3i.png"%(n)
      imsave(fname,imread(files+nlist[i][j]))
      n+=1
      


def cropLightField(lf):
  
  emptyDist_y = 0
  emptyDist_x = 0
  
  lf_array = np.sum(np.copy(lf.lf[:].astype(np.float32)),axis=4)
  
  for x in range(lf.xRes):
    if np.sum(lf_array[0,0,:,x])>0:
      if x%2 != 0:
        x+=1
      emptyDist_x = x
      break
    
  for y in range(lf.yRes):
    if np.sum(lf_array[lf.vRes-1,0,y,:])>0:
      if y%2 != 0:
        y+=1
      emptyDist_y = y
      break
      
  lf_array = np.copy(lf.lf[:])[:,:,emptyDist_y:,emptyDist_x:,:]
  
  lf_out = LightField()
  lf_out.lf = lf_array[:]
  lf_out.vRes = lf.vRes
  lf_out.hRes = lf.hRes
  lf_out.yRes = lf_array.shape[2]
  lf_out.xRes = lf_array.shape[3]
  lf_out.channels = lf_array.shape[4]
  lf_out.dH = lf.dH
  lf_out.dV = lf.dV
  lf_out.vSampling = lf.vSampling
  lf_out.hSampling = lf.hSampling
  
  return lf_out
        
        

  
  
  
  
  
  
    
    
      