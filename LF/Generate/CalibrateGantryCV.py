import os
import sys
from LF.Helpers import *
from LF.settings import *
from LF.Base.LightField import *
from LF.Generate.GantryEquipment import reorderImagesFromGantry, Cam, Gantry, cropLightField


try: import numpy as np
except: 
    print "seems you haven't installed numpy: on Ubuntu you can do this via($ sudo apt-get install python-numpy)"
    exit()
try: import cv
except: 
    print "seems you haven't installed pyopencv: visit http://opencv.org/"
    exit()
try: 
    from scipy.misc import imread, imsave
except: 
    print "seems you haven't installed scipy: on Ubuntu you can do this via($ sudo apt-get install python-scipy)"
    exit()
try: 
    import matplotlib.image as mpimg
except: 
    print "seems you haven't installed matplotlib: on Ubuntu you can do this via($ sudo apt-get install python-matplotlib)"
    exit()
try: 
    import h5py as h5
except: 
    print "seems you haven't installed h5py: on Ubuntu you can do this via($ sudo apt-get install python-h5py"
    exit()


COLUMNS = 8
ROWS = 6
CHECKER_SIZE = 7.6
GRID = (9,9)


def getCheckerboardMarkersCV(path, columns, rows, saveMarkers=None,subfolder=None):
  '''!
  @brief get the corner positions of a calibration checker board (subpixel accuracy) for the openCV target 
  @author Sven Wanner
  @param path:<str> path to internal calib images
  @param columns:<int> number of checker columns
  @param rows:<int> number of checker rows
  @param saveMarkers:<str>[None] location to store drawn markers
  '''
  dlist = os.listdir(path)
  dlist.sort()
  
  markerPos=[]
  
  if not path.endswith("/"): path+="/"
  
  if DEBUG>0: print "search corners:"
  for i,im in enumerate(dlist):
    impath = path+im
    
    #open internal calibration image and init output container
    tmpIm = cv.LoadImage(impath, cv.CV_LOAD_IMAGE_GRAYSCALE)
    tmpIm = cv.LoadImage(impath, cv.CV_LOAD_IMAGE_GRAYSCALE)
    
    #search checker board corners
    corners=cv.FindChessboardCorners(tmpIm, (columns,rows) ,cv.CV_CALIB_CB_ADAPTIVE_THRESH )
      
    if corners[0]==1:
      if len(corners[1]) == columns*rows:
        if DEBUG>0: print ".",
      
      if saveMarkers is not None and type(saveMarkers) == type(""):
        loc = ensure_dir(saveMarkers,subfolder)
        debugIm = cv.LoadImage(impath, cv.CV_LOAD_IMAGE_COLOR)
        cv.DrawChessboardCorners(debugIm, (columns,rows), corners[1], 1)
        ImName = loc+"corners_"+str(i)+".png"
        cv.SaveImage(ImName,debugIm)
      
      subcorners=cv.FindCornerSubPix(tmpIm, corners[1], (5,5), (-1,-1), (cv.CV_TERMCRIT_ITER, 100, 0))
      t=[]
      for e in range(0,len(subcorners)):
          t.append((subcorners[e][0],subcorners[e][1]))
      markerPos.append(t)
    else:
      print "\ncorner detection failed in: ",im,"!\n"
  print "\n"
    
  return markerPos
  


def calibrateGantryCV(internal_path, external_path, columns=COLUMNS, rows=ROWS, grid=GRID, saveMarkers=None, saveCalibFile=None):
  """
  @author: Sven Wanner
  @brief: calibrates camera and gantry objects and returns them
  @param internal_path:<str> path to internal calibration images
  @param external_path:<str> path to external calibration images
  @param columns:<int>[8] number of horizontal checkerboard crossings
  @param rows:<int>[6] number of vertical checkerboard crossings 
  @param grid:<list>[(9,9)] lf grid size
  @param calibFile:<str>[None] filepath to save cam and gantry objects
  @param saveMarkers:<str>[None] filepath to save drawn checker markers
  """
  
  orig_path = external_path
  if external_path[-1] == "/":
    external_path = external_path[:-1]
  external_path+="_reordered"
  
  try: #try to read reordered external filenames
    elist = os.listdir(external_path)
    elist.sort()
    if len(elist)==0: raise Exception("")
  except:
    external_path = ensure_dir(external_path)
    reorderImagesFromGantry(orig_path,external_path,grid)
    elist = os.listdir(external_path)
    elist.sort()
    
  if not external_path.endswith("/"): external_path+="/"
  
  #read and sort intern and extern filenames
  ilist = os.listdir(internal_path)
  ilist.sort()
  
  #get marker points from all images
  markerPos_external = getCheckerboardMarkersCV(external_path, columns, rows, saveMarkers=saveMarkers,subfolder="/external")
 
  if DEBUG>0: 
    print "num of cams:", len(markerPos_external)
    print "num of 3D points:",len(markerPos_external[len(markerPos_external)/2])
  if(rows*columns == len(markerPos_external[len(markerPos_external)/2])):
    if DEBUG>0: print "all corners found"
  else:
    raise Exception("not all corners found, check input!")
    
    
  #center ref image
  center = len(elist)/2
  refName = external_path+elist[center]
  if DEBUG>0: print "center:",center,"reference image:",refName
  refIm = imread(refName)

  
  CheckerSize = CHECKER_SIZE
  target_points=np.zeros( (columns*rows,3), dtype=np.float64)
  X,Y = np.meshgrid(np.arange(columns)*CheckerSize,np.arange(rows)*CheckerSize)
  target_points[:,0] = X.flatten()
  target_points[:,1] = Y.flatten()
  
  object_points = np.zeros( ((len(elist)+len(ilist))*columns*rows,3), dtype=np.float64)
  for e in range(len(elist)+len(ilist)):
    for i in range(columns*rows):
      object_points[e*columns*rows+i,0] = target_points[columns*rows-1-i,1] #markerPos_external[center][i][0]
      object_points[e*columns*rows+i,1] = target_points[i,0] #markerPos_external[center][i][1]
      
  #cv target requires that all points are found
  pointCounts = cv.CreateMat(len(elist)+len(ilist),1, cv.CV_32SC1)
  cv.Set(pointCounts,columns*rows)

  markerPos_internal = getCheckerboardMarkersCV(internal_path, columns, rows, saveMarkers=saveMarkers,subfolder="/internal")
  
  imagePoints = cv.CreateMat( (len(elist)+len(ilist))*columns*rows,2, cv.CV_32FC1)
  
  ioffset = 0
  for i in range(len(markerPos_internal)):
    for e in range(len(markerPos_internal[i])):
      imagePoints[ioffset,0] = markerPos_internal[i][e][0]
      imagePoints[ioffset,1] = markerPos_internal[i][e][1]
      ioffset += 1
  if DEBUG>0: print "found internal points"
  
  eoffset=ioffset
  for i in range(len(markerPos_external)):
    for e in range(len(markerPos_external[i])):
      imagePoints[eoffset,0]=markerPos_external[i][e][0]
      imagePoints[eoffset,1]=markerPos_external[i][e][1]
      eoffset+=1
  if DEBUG>0: print "found external points"
  
  imageSize = (np.shape(refIm)[1],np.shape(refIm)[0])
  cmat = cv.CreateMat(3,3,cv.CV_32FC1)
  cv.Set(cmat,0)
  distCoeffs = cv.CreateMat(5,1,cv.CV_32FC1)
  cv.Set(distCoeffs,0)
  rvecs  = cv.CreateMat(len(ilist)+len(elist), 3, cv.CV_32FC1)
  cv.Set(rvecs,0)
  tvecs = cv.CreateMat(len(ilist)+len(elist), 3, cv.CV_32FC1)
  cv.Set(tvecs,0)
  
  if DEBUG>0: print "Bundle Adjustment...."
  cv.CalibrateCamera2(cv.fromarray(object_points), imagePoints, pointCounts, imageSize, cmat, distCoeffs, rvecs, tvecs)#, cv.CV_CALIB_FIX_PRINCIPAL_POINT+cv.CV_CALIB_FIX_ASPECT_RATIO)

  Homs=[]
  Rot=[]
  Trans=[]
  minTransXY=10000000
  
  for i in range(len(ilist),len(ilist)+len(elist)):
    Hom=cv.CreateMat(3,3, cv.CV_32FC1)
    tmpRot=cv.CreateMat(1,3, cv.CV_32FC1)
    tmpTrans=cv.CreateMat(1,3, cv.CV_32FC1)
    tmpRot[0,0]=rvecs[i,0]
    tmpRot[0,1]=rvecs[i,1]
    tmpRot[0,2]=rvecs[i,2]
    tmpTrans[0,0]=tvecs[i,0]
    tmpTrans[0,1]=tvecs[i,1]
    tmpTrans[0,2]=tvecs[i,2]
    cv.Rodrigues2(tmpRot,Hom)
    Hom[0,2]=(tvecs[i,1]-tvecs[center,1])/(tvecs[center,2])#set center view as reference
    Hom[1,2]=(tvecs[i,0]-tvecs[center,0])/(tvecs[center,2])#set center view as reference
    Hom[2,2]=1
    Homs.append(np.asarray(Hom,dtype=float))
    Rot.append(np.asarray(tmpRot,dtype=float))
    Trans.append(np.asarray(tmpTrans,dtype=float))
    transDist=np.sqrt(tmpTrans[0,0]*tmpTrans[0,0]+tmpTrans[0,1]*tmpTrans[0,1])
    if transDist < minTransXY:
      minTransXY=transDist
      
  if minTransXY > imageSize[0]/2 or minTransXY > imageSize[1]/2:
    print "-------------------------------------------------------------------------------------------------------------------------------------*"
    print "WARNING: ref pos outside of image: check calibration sources! [usually insufficient internal calibration]",minTransXY,imageSize[0]/2,imageSize[1]/2
    print "-------------------------------------------------------------------------------------------------------------------------------------*"
  
  GantryObject = Gantry()
  GantryObject.grid=grid
  GantryObject.trans=Trans
  GantryObject.rot=Rot
  GantryObject.homs=Homs

  rCam = Cam()
  rCam.cmat=np.asarray(cmat)
  rCam.dist=np.asarray(distCoeffs)
  rCam.rot=np.asarray(rvecs)
  rCam.trans=np.asarray(tvecs)


  if saveCalibFile is not None:
    GantryObject.save(saveCalibFile+".gantry")
    rCam.save(saveCalibFile+".cam")
    if DEBUG>0: 
      print "cam object saved at:",saveCalibFile+".cam"
      print "gantry object saved at:",saveCalibFile+".gantry"

  return GantryObject, rCam  
  
  
def genLFfromGantry(path, CamObject, GantryObject, crop=True):
    """
    @author: Sven Wanner
    @brief: generates light field instance from calibration gantry objects
    @param path:<str> path to gantry images
    @param CamObject:<instance> Cam object instance
    @param GantryObject:<instance> Gantry obkect instance     
    """
    if DEBUG>0: print "reading LF from ", path

    LFres = LightField()
    GObject = GantryObject.copy()
    
    orig_path = path
    if path.endswith("/"): path = path[:-1]
    path+="_reordered"
    
    if not path.endswith("/"): path+="/"
    
    try: #try to read reordered external filenames
      dlist = os.listdir(path)
      dlist.sort()
      if len(dlist)==0: raise Exception("")
    except:
      path = ensure_dir(path)
      reorderImagesFromGantry(orig_path,path,GantryObject.grid)
      dlist = os.listdir(path)
      dlist.sort()
    
    
    #center ref image
    center=len(dlist)/2
    refName = path+dlist[center]
    if DEBUG>0: print "center:",center,"image:",refName
    refIm = imread(refName)

    if DEBUG>0: print "grid size", GObject.grid
    LFres.vSampling = np.zeros( GObject.grid, dtype=float )
    LFres.uSampling = np.zeros( GObject.grid, dtype=float )

    tmpName = path+dlist[0]
    tmpIm = imread(tmpName).astype(np.float32)
    LFres.lf = np.zeros( (GObject.grid[0],GObject.grid[1],np.shape(tmpIm)[0], np.shape(tmpIm)[1], 3), dtype='uint8' )

    LFres.vRes = GObject.grid[0]
    LFres.hRes = GObject.grid[1]
    LFres.yRes = np.shape(tmpIm)[0]
    LFres.xRes = np.shape(tmpIm)[1]
    if DEBUG>0: print "generating LF of size:", LFres.vRes, LFres.hRes,LFres.yRes, LFres.xRes,3, "=", (LFres.vRes*LFres.hRes*LFres.yRes*LFres.xRes*3)/1000000, "MB"

    avV = 0
    avU = 0
    uMax = -10000
    vMax = -10000
    uMin = 10000
    vMin = 10000
        
    #convention: 
    #L2R_T2B: row first: Left to Right and Top to Bottom
    #RlTd
    
    n=0
    for i in range(0,len(dlist)):
      if DEBUG==1: print "\rprocess image",i,
      vpos = 0
      hpos = 0
      tmpz=GObject.trans[i][0,2] #GObject.homs[i][2,2]

      
      vpos = (int(i)/GObject.grid[1])
      hpos = n
      #GObject.homs[i][1,2]*=-1
      GObject.homs[i][0,2]*=-1

      n+=1
      if n==GObject.grid[0]:
        n=0
      if DEBUG>1:
        print "(GObject.grid[0],GObject.grid[1])=(",GObject.grid[0],",",GObject.grid[1],")"
        print "(vpos,hpos)=(",vpos,",",hpos,")"
        print "(GObject.homs[",i,"][1,2],GObject.homs[0,2])=(",GObject.homs[i][1,2],",", GObject.homs[i][0,2],")"
        
      LFres.vSampling[vpos,hpos]=GObject.homs[i][0,2]*tmpz
      avV += GObject.homs[i][0,2]*tmpz    
      if GObject.homs[i][0,2]*tmpz > vMax:
        vMax = GObject.homs[i][0,2]*tmpz
      if GObject.homs[i][0,2]*tmpz < vMin:
        vMin = GObject.homs[i][0,2]*tmpz
      LFres.uSampling[vpos,hpos]=GObject.homs[i][1,2]*tmpz
      avU += GObject.homs[i][1,2]*tmpz
      if GObject.homs[i][1,2]*tmpz > uMax:
        uMax = GObject.homs[i][1,2]*tmpz
      if GObject.homs[i][1,2]*tmpz < uMin:
        uMin = GObject.homs[i][1,2]*tmpz

      tmpName = path+dlist[i]
      if DEBUG>1: 
        print "load image:",dlist[i]
        print "--------------------------------------------------------------"
        
      tmpIm = imread(tmpName).astype(np.float32)
      


      cvOut = cv.CreateMat(tmpIm.shape[1], tmpIm.shape[0], cv.CV_32FC1)
      cvFinal = cv.CreateMat(tmpIm.shape[1], tmpIm.shape[0], cv.CV_32FC1)
      
      map1 = cv.CreateMat(tmpIm.shape[1], tmpIm.shape[0],cv.CV_32FC1)
      map2 = cv.CreateMat(tmpIm.shape[1], tmpIm.shape[0],cv.CV_32FC1)
  
      cv.InitUndistortRectifyMap(cv.fromarray(CamObject.cmat), cv.fromarray(CamObject.dist), cv.fromarray(GObject.homs[i]), cv.fromarray(CamObject.cmat), map1, map2)

      #convert numpy array to cvMat
      cvOut = cv.fromarray(tmpIm)

      cv.Remap(cvOut, cvFinal, map1, map2)
      
      #convert cvMat to numpy array
      tmpIm = np.asarray(cvFinal)
      
      tmpRes = np.fliplr(np.flipud(np.rot90(tmpIm))) #flip

      tmpRes=((tmpRes-np.min(tmpRes))/(np.max(tmpRes)-np.min(tmpRes)))*255
      LFres.lf[vpos,hpos,:,:,0]=tmpRes.astype(np.uint8)
      LFres.lf[vpos,hpos,:,:,1]=tmpRes.astype(np.uint8)
      LFres.lf[vpos,hpos,:,:,2]=tmpRes.astype(np.uint8)
      
    avV /= len(dlist)
    avU /= len(dlist)

    if DEBUG>1:
      print "vsampling:",vMin,vMax
      print "usampling:",uMin,uMax
      print "avV:",avV
      print "avU:",avU 

    LFres.dH = (uMax-uMin)/GObject.grid[1]
    LFres.dV = (vMax-vMin)/GObject.grid[0]
    LFres.channels = 3

    if crop:
      lf_out = cropLightField(LFres)
      return lf_out
    else:
      return LFres
  
  

def customLightFieldFromGantry(path,data,crop=True):
    
  intern = path+"/intern/"
  extern = path+"/extern/"
  images = path+"/"+data+"/"
  output = path+"/calib"
  markerOut = path+"/markers"
  
  try:
    cam = Cam().load(path+"/calib.cam")
    gantry = Gantry().load(path+"/calib.gantry")
  except:
    print "\nno cam and gantry objects found..."
    print "calculate gantry and cam ..."
    gantry,cam = calibrateGantryCV(intern,extern,8,6,grid=(9,9),saveMarkers=markerOut,saveCalibFile=output)
  
  
  lf = genLFfromGantry(images,cam,gantry,crop)
  lf.save(path+"/"+data+".h5")
  
  return lf
  
        
      