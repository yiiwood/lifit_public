import types
import libimproc as fast

try: import numpy as np
except: 
    print "seems you haven't installed numpy: on Ubuntu you can do this via($ sudo apt-get install python-numpy)"
    exit()
try: 
    import Image
except: 
    print "seems you haven't installed PIL: on Ubuntu you can do this via($ sudo pip install PIL"
    exit()



def Image2numpy(img):
  return np.asarray(img)

def numpy2Image(img):
  return Image.fromarray(np.uint8(img))


def reduceChannels(img,type="rgb2bw"):
  """
  @author: Sven Wanner
  @brief: reduces color channels to scalar depending on reduction model
  @param img:<ndarray> input rgb image
  @param type:<str>[rgb2bw] conversion type
  @return: <2D ndarray>
  """
  out = np.zeros((img.shape[0],img.shape[1]),dtype=np.float32)
  
  if type == "rgb2bw":
    out[:] = 0.3*img[:,:,0]+0.59*img[:,:,1]+0.11*img[:,:,2]
  return out



def colorRange(arr,newRange=[0,255]):
  """
  @brief: adjust the value range of a ndarray to the newRange
  @param arr: <ndarray> input array
  @param newRange: <int list>[0,255] new color range
  @return: <ndarray>
  """
  amin = np.amin(arr)
  amax = np.amax(arr)
  if amin == amax:
    return arr
  else:
    oldRange = [amin,amax]
    oldDiff = oldRange[1] - oldRange[0]
    newDiff = newRange[1] - newRange[0]
    out = (arr - oldRange[0]) / oldDiff * newDiff + newRange[0]
    return out
  
  
  
##########################################################################################################################
################ B E   C A R E F U L    W H E N    U S I N G    N O T    F U L L Y   T E S T E D    ######################
##########################################################################################################################
  
  
  
  
def colormap(intensities,cmap):
  return fast._colormap(intensities,cmap)
  

def rgbLF2hsvLF(lf_array,lf_out):
  """
  @brief: converts rgb lf to hsv space
  @param lf_array:<5D ndarray> input array
  @param lf_out:<5D ndarray> input array
  """
  if lf_array.shape != lf_out.shape:
    raise Exception("shape mismatch in rgbLF2hsvLF!")
  if lf_out.dtype != np.float32:
    raise Exception("output ndarray needs to be of type float32!")
  from time import time
  t0 = time()
  fast._rgbLF2hsvLF(lf_array.astype(np.float32),lf_out)
  print "duration:",time()-t0,"s"
  
  
def hsvLF2rgbLF(lf_array,lf_out):
  """
  @brief: converts hsv lf to rgb space
  @param lf_array:<5D ndarray> input array
  @param lf_out:<5D ndarray> input array
  """
  if lf_array.shape != lf_out.shape:
    raise Exception("shape mismatch in hsvLF2rgbLF!")
  from time import time
  t0 = time()
  fast._hsvLF2rgbLF(lf_array.astype(np.float32),lf_out)
  print "duration:",time()-t0,"s"
  
  
  
def anisotropicDiffusion(im,iteration,kappa,delta_t):
  """
  anisotropic diffusion based on malik
  """
  return fast._anisotropicDiffusion(im,iteration,kappa,delta_t)

 
    
def tv_regularizer(im, weight=0.5, n_iter_max=200, p=1):
  """
  tv regularizing
  """
  cannels = 0  
  
  im = im.astype(np.float32)
  
  if len(im.shape)==2:
    tmp = np.zeros((im.shape[0],im.shape[1],1),dtype=np.float32)
    tmp[:,:,0] = im[:]
    im = np.copy(tmp)
    channels = 1
    
  if len(im.shape)==3:
    channels = im.shape[2]
    u = np.zeros((im.shape[0],im.shape[1],channels),dtype=np.float32)
    old_Range = []
  
    for i in range(channels):
      old_Range.append([np.amin(im[:,:,i]),np.amax(im[:,:,i])])
      
    
    for channel in range(channels):
      if type(weight) is types.IntType or type(weight) is types.FloatType:
      
        xi_x = np.zeros_like(im[:,:,channel]).astype(np.float32)
        xi_y = np.zeros_like(im[:,:,channel]).astype(np.float32)
        q = np.zeros_like(im[:,:,channel]).astype(np.float32)
        
        lamda = weight
        
        tau = 1.0/np.sqrt(8.0)
        
        u[:,:,channel] = np.copy(im[:,:,channel]).astype(np.float32)
        
        i=0
        while i < n_iter_max:
          gy,gx = fast._grad(u[:,:,channel])
          xi_y += tau*gy
          xi_x += tau*gx
          xi_y,xi_x = fast._normP(xi_y,xi_x)
          
          q = q[:] + tau*(u[:,:,channel]-im[:,:,channel])
          q = fast._normQ(q.astype(np.float32),lamda,tau,p)

          div = fast._div(xi_y,xi_x)
          u[:,:,channel] = u[:,:,channel] + tau*(div[:] - q[:])
          
          u[:,:,channel] = colorRange(u[:,:,channel],old_Range[channel])
      
          i+=1      
    if channels == 1:
      return u[:,:,0]
    else:
      return u
  
  else:
    print "ERROR: tv_weightDenoise need scalar weight!"
    return None





def tv_weightDenoise(im, weight=None, n_iter_max=100, lamda=1):
  """
  tv denoising using a weighted reprojection
  """
  if len(im.shape)!=2:
    print "ERROR: tv_weightDenoise needs 2d image array!"
    return None
    
  xi_x = np.zeros_like(im)
  xi_y = np.zeros_like(im)
  
  if (str(type(weight)) == "<type 'numpy.ndarray'>") and (len(weight.shape)==2):
    if np.amax(weight) > 1 or np.amin(weight) < 0:
      print "WARNING: tv_weightDenoise arg weight was rescaled  ->  [0,1]"
      weight = colorRange(weight,newRange=[0,1])
  else:
    print "WARNING: tv_weightDenoise need 2d ndarray as weight! weight is set to np.ones!"
    weight = np.zeros_like(im)
    weight[:,:] = 1
 
  tau = 1.0/(4.0*lamda+1e-3)
  
  u = np.copy(im)
  
  i=0
  while i < n_iter_max:

    div = fast._div(xi_y,xi_x)
    u = u - lamda * div
    gy,gx = fast._grad(u)
    xi_y -= tau*gy
    xi_x -= tau*gx
    xi_y,xi_x = fast._weightNormP(weight,xi_y,xi_x)
    i+=1
  
  return u
  