cd ./LF/Depth
python setup.py build_ext --inplace
cd ./../ImageProcessing
python setup.py build_ext --inplace

cd ./../Viewer
pyuic4 viewer.ui > viewerUI.py
pyuic4 previewer.ui > previewerUI.py

cp viewer.py viewLF
chmod +x viewLF

sudo cp viewLF /usr/local/bin/viewLF
